window.addEventListener('load', function() {

    // 轮播图
    function FOCUS() {
        // 变量声明
        var arrow_l = document.querySelector('.arrow_l');
        var arrow_r = document.querySelector('.arrow_r');
        var focus = document.querySelector('.focus');
        var focusWidth = focus.offsetWidth;
        // 隐藏箭头 鼠标经过显示
        focus.addEventListener('mouseenter', function() {
            arrow_l.style.display = 'block';
            arrow_r.style.display = 'block';
            clearInterval(timer);
            timer = null;
        });
        focus.addEventListener('mouseleave', function() {
            arrow_l.style.display = 'none';
            arrow_r.style.display = 'none';
            timer = setInterval(function() {
                arrow_r.click();
            }, 2000);
        });

        // 动态生成小圆点
        var ul = focus.querySelector('ul');
        var ol = focus.querySelector('.circle');
        var index = 0;
        // 已经固定为不同种类照片数的长度
        var length = ul.children.length;
        for (var i = 0; i < length; i++) {
            var li = this.document.createElement('li');
            // 为每一个小圆点设置属性
            li.setAttribute('index', i);
            ol.appendChild(li);
            li.addEventListener('click', function() {
                for (var i = 0; i < length; i++) {
                    ol.children[i].className = '';
                }
                // 点击小圆点变色
                this.className = 'current';
                // // 小圆点切换图片功能
                index = this.getAttribute('index');
                var target = -index * focusWidth;
                num = index;
                circle = index;
                animate(ul, target);
            });
        }

        ol.children[0].className = 'current';
        var num = 0;
        var circle = 0;
        var flag = true;
        var first = ul.children[0].cloneNode(true);
        ul.appendChild(first);

        // 右侧箭头
        arrow_r.addEventListener('click', function() {
            if (flag) {
                flag = false;
                if (num == length) {
                    ul.style.left = 0 + 'px';
                    num = 0;
                }
                num++;
                animate(ul, -num * focusWidth, function() {
                    flag = true;
                });
                // 小圆点随右侧箭头的变化
                circle++;
                if (circle == length) {
                    circle = 0;
                }
                circleChange();
            }
        });

        // 左侧箭头
        arrow_l.addEventListener('click', function() {
            if (flag) {
                flag = false;
                if (num == 0) {
                    ul.style.left = -length * focusWidth + 'px';
                    num = length;
                }
                num--;
                animate(ul, -num * focusWidth, function() {
                    flag = true;
                });
                // 8. 小圆点随左侧箭头的变化
                circle--;
                if (circle < 0) {
                    circle = length - 1;
                }
                circleChange();
            }
        });

        function circleChange() {
            for (var i = 0; i < length; i++) {
                ol.children[i].className = '';
            }
            ol.children[circle].className = 'current';
        }

        var timer = this.setInterval(function() {
            arrow_r.click();
        }, 2000);
    }

    // 获取所有专利概要 并且给‘每一个’专利概要注册点击事件的函数
    function clickPatentOutline() {

        // 专利概要的变量声明
        let patentOutlines = document.querySelectorAll('.patentOutline'); //第一次加载时的所有专利概要 
        let image1s = document.querySelectorAll('.image1'); // ‘所有’的专利概要中的照片1
        let image2s = document.querySelectorAll('.image2'); // ‘所有’的专利概要中的照片2
        let image3s = document.querySelectorAll('.image3'); // ‘所有’的专利概要中的照片3
        let patentNames = document.querySelectorAll('.patentName'); // ‘所有’的专利名
        let patentTypes = document.querySelectorAll('.patentType'); // ‘所有’的专利类型
        let patentLabels = document.querySelectorAll('.patentLabel'); // 所有的专利标签
        let introduces = document.querySelectorAll('.introduce'); // 所有的专利介绍

        let patentName = document.querySelector('#patentName'); // 详细的专利名
        let introduce = document.querySelector('#introduce'); // 详细的专利介绍
        let image1ID = document.querySelector('#image1-id'); // 详细的图片1
        let image2ID = document.querySelector('#image2-id'); // 详细的图片2
        let image3ID = document.querySelector('#image3-id'); // 详细的图片3
        let type = document.querySelector('#type'); // 详细的专利类型
        let label = document.querySelector('#label'); // 详细的专利标签

        let mask = document.querySelector('.mask'); // 遮罩层
        let form = document.querySelector('form'); // 表单

        for (let i = 0; i < patentOutlines.length; i++) {
            patentOutlines[i].onclick = function() {
                // 表单显示
                form.style.display = 'block';
                //遮罩层显示
                mask.style.display = 'block';
                // 将被点击的概要的信息填写到表单上
                patentName.value = patentNames[i].innerHTML;
                introduce.value = introduces[i].innerHTML;
                type.value = patentTypes[i].innerHTML;
                label.value = patentLabels[i].innerHTML;
                image1ID.src = image1s[i].src;
                image2ID.src = image2s[i].src;
                image3ID.src = image3s[i].src;
            }
        }
    }

    // 点击返回按钮
    function clickRevertBtn() {
        // 变量声明
        let revert = document.querySelector('#revert'); // 返回按钮

        let mask = document.querySelector('.mask'); // 遮罩层
        let form = document.querySelector('form'); // 表单

        // 点击返回按钮
        revert.addEventListener('click', function() {
            // 隐藏遮罩层
            mask.style.display = 'none';
            // 隐藏表单
            form.style.display = 'none';
        });
    }

    // 发送请求
    const xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://localhost:8000/approved_pantents');
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded'); // 后台需要的是json类型的字符串
    xhr.send(null);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if (xhr.status >= 200 && xhr.status < 300) {
                let message = JSON.parse(xhr.response).message;
                console.log(message)

                // 轮播图
                // 更换轮播图的图片为最新专利的图片
                let imgs = document.querySelectorAll('.banner>ul>li>a>img');
                imgs[0].src = message[message.length - 1].image1;
                imgs[1].src = message[message.length - 1].image2;
                imgs[2].src = message[message.length - 1].image3;
                FOCUS();

                // 将数据库中的信息加载到页面上
                // 此时页面只有一个模板
                // 专利概要的变量声明
                for (let i = 0; i < message.length; i++) {

                    // 变量声明
                    let patentOutlines = document.querySelectorAll('.patentOutline'); //第一次加载时的所有专利概要 
                    let patentContainer = document.querySelector('.patent-container'); // 展示所有专利信息的地方
                    // 克隆模板专利概要
                    let patentOutline = patentOutlines[0].cloneNode(true);
                    patentOutline.style.display = 'flex';
                    // 从后面开始获取信息 将每一个信息都加载到模板的下面
                    if (patentOutlines.length === 1) {
                        patentContainer.appendChild(patentOutline);
                    } else {
                        patentContainer.insertBefore(patentOutline, patentOutlines[1]);
                    }
                    // 信息填充
                    let image1s = document.querySelectorAll('.image1'); // ‘所有’的专利概要中的照片1
                    let image2s = document.querySelectorAll('.image2'); // ‘所有’的专利概要中的照片2
                    let image3s = document.querySelectorAll('.image3'); // ‘所有’的专利概要中的照片3
                    let patentNames = document.querySelectorAll('.patentName'); // ‘所有’的专利名
                    let patentTypes = document.querySelectorAll('.patentType'); // ‘所有’的专利类型
                    let patentLabels = document.querySelectorAll('.patentLabel'); // 所有的专利标签
                    let introduces = document.querySelectorAll('.introduce'); // 所有的专利介绍

                    image1s[1].src = message[i].image1;
                    image2s[1].src = message[i].image2;
                    image3s[1].src = message[i].image3;
                    patentNames[1].innerHTML = message[i].name;
                    patentTypes[1].innerHTML = message[i].type;
                    patentLabels[1].innerHTML = message[i].label;
                    introduces[1].innerHTML = message[i].introduce;
                }

                // 专利查看模块
                // 给返回按钮注册点击事件
                clickRevertBtn();
                // 给专利概要注册点击事件
                clickPatentOutline();
                // 给轮播图注册点击事件
                for (let i = 0; i < imgs.length; i++) {
                    imgs[i].addEventListener('click', function() {
                        let patentOutlines = document.querySelectorAll('.patentOutline'); //第一次加载时的所有专利概要 
                        patentOutlines[0].click();
                    });
                }
            }
        }
    }
});