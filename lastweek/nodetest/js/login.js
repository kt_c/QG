window.onload = function() {

    // 变量声明
    let idCard = document.querySelector('#id-card'); // 身份证
    let password = document.querySelector('#password'); // 密码
    let loginBtn = document.querySelector('#login-btn'); // 登录按钮

    let withoutIDCard = document.querySelector('.id-card .without'); // 没有身份证
    let withoutPassword = document.querySelector('.password .without'); // 没有密码

    let errorIDCard = document.querySelector('.id-card .errorIDCard'); // 错误身份证
    let errorPassword = document.querySelector('.password .errorPassword'); // 错误密码
    let flag = 1;

    // 表单验证
    loginBtn.addEventListener('click', function() {
        // 用于记录正确填写的个数
        flagTest = 0;
        // 身份证号为空
        if (isNull(idCard.value)) {
            errorIDCard.style.display = 'none';
            withoutIDCard.style.display = 'block';
            flagTest = 0;
        }
        // 身份证号错误 
        else if (!checkIDCard(idCard.value)) {
            withoutIDCard.style.display = 'none';
            errorIDCard.style.display = 'block';
            flagTest = 0;
        }
        // 身份证号正确 
        else {
            withoutIDCard.style.display = 'none';
            errorIDCard.style.display = 'none';
            flagTest++;
        }
        // 密码为空
        if (password.value == ``) {
            errorPassword.style.display = 'none';
            withoutPassword.style.display = 'block';
            flagTest = 0;
        }
        // 密码格式错误
        else if (!isPassword(password.value)) {
            withoutPassword.style.display = 'none';
            errorPassword.style.display = 'block';
            flagTest = 0;
        }
        // 密码正确
        else {
            withoutPassword.style.display = 'none';
            errorPassword.style.display = 'none';
            flagTest++;
        }
        if (flagTest === 2) {
            let data = new Object();
            data.idCard = `${idCard.value}`;
            data.password = `${password.value}`;
            // data.authorization = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZENhcmQiOiI0NDA3ODUyMDAzMDEzMDE2MTciLCJpYXQiOjE2NTEyODU1ODUsImV4cCI6MTY1MTI4OTE4NX0.jbMvbbuSjbcWDlkVkiIG_ObZYCb_zhNgrGi0Jr1VMqA';

            const xhr = new XMLHttpRequest();
            xhr.open('POST', 'http://localhost:8000/login');
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded'); // 后台需要的是json类型的字符串
            // console.log(data);
            xhr.send(JSON.stringify(data));
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200 && xhr.status < 300) {
                        let response = JSON.parse(xhr.response);
                        if (JSON.parse(xhr.response).status === 0) {
                            // 将token存储到本地
                            localStorage.setItem('patent', JSON.stringify({
                                idCard: data.idCard,
                                user: 'users',
                                token: response.token,
                            }))
                            alert('登录成功!!');
                            window.location.href = '../views/index.html';
                        } else if (JSON.parse(xhr.response).status === 1) {
                            alert('注册一个账号吧，此账号不存在');
                        } else if (JSON.parse(xhr.response).status === 2) {
                            alert('密码错误');
                        } else if (JSON.parse(xhr.response).status === 3) {
                            localStorage.setItem('patent', JSON.stringify({
                                idCard: data.idCard,
                                user: `administrators`,
                                token: response.token,
                            }))
                            alert('管理员账号登录成功');
                            window.location.href = '../views/super-index.html';
                        }
                    }
                }
            }
        }
        flag = 1;
    });
}