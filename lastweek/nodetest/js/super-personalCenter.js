window.onload = function() {

    // 个人信息模块
    function personalInfo() {
        // 变量声明
        let infoReviseBtn = document.querySelector('.personal-container .reviseBtn'); // 个人信息的修改按钮
        let infoSaveBtn = document.querySelector('.personal-container .saveBtn'); // 个人信息的保存按钮
        let mask = document.querySelector('.mask'); // 个人资料表面的遮罩层
        let infoInputs = document.querySelectorAll('.personal-container input'); // 个人资料的修改框 邮箱地址 电话号码

        // 点击个人信息的修改按钮  
        infoReviseBtn.addEventListener('click', function() {
            // 透明遮罩层消失
            mask.style.display = 'none';
            // 输入框出现
            for (let i = 0; i < infoInputs.length; i++) {
                infoInputs[i].style.border = '1px solid #666';
            }
            // 修改按钮隐藏
            infoReviseBtn.style.display = 'none';
            // 保存按钮显示
            infoSaveBtn.style.display = 'block';
        });

        // 点击个人信息的保存按钮
        infoSaveBtn.addEventListener('click', function() {
            // 透明遮罩层显示
            mask.style.display = 'block';
            // 输入框消失
            for (let i = 0; i < infoInputs.length; i++) {
                infoInputs[i].style.border = 'none';
            }
            // 修改按钮显示
            infoReviseBtn.style.display = 'block';
            // 保存按钮隐藏
            infoSaveBtn.style.display = 'none';
        });
    }

    // 发送请求返回用户数据
    function request() {
        return new Promise((resolve, reject) => {
            let data = new Object();
            let patent = JSON.parse(localStorage.getItem('patent'));
            data.authorization = patent.token;
            data.idCard = patent.idCard;
            data.user = patent.user;
            // console.log(data);
            const xhr = new XMLHttpRequest();
            xhr.open('POST', 'http://localhost:8000/personal');
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded'); // 后台需要的是json类型的字符串
            xhr.send(JSON.stringify(data));
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200 && xhr.status < 300) {
                        let response = JSON.parse(xhr.response);
                        resolve(response);
                    }
                }
            }
        });
    }

    // 用户数据渲染页面
    function render(data) {

        // 个人信息
        let account = document.querySelector('.account>p'); // 账户
        let postCode = document.querySelector('.postCode>p'); // 邮政编码
        let address = document.querySelector('.address>p'); // 详细地址
        let indetityNumber = document.querySelector('.identityNumber>p'); // 证件号码
        let province = document.querySelector('.province>p'); // 省份
        let email = document.querySelector('.email>input'); // 邮箱
        let name = document.querySelector('.name>p'); // 姓名
        let nationality = document.querySelector('.nationality>p'); // 国籍
        let city = document.querySelector('.city>p'); // 县
        let phoneNumber = document.querySelector('.phoneNumber>input'); // 电话号码

        account.innerHTML = data.info[0].idCard; // 账户
        postCode.innerHTML = data.info[0].postCode; // 邮编
        address.innerHTML = data.info[0].house_number; // 详细地址
        indetityNumber.innerHTML = data.info[0].idCard; // 证件号码
        province.innerHTML = data.info[0].province; // 省份
        email.innerHTML = data.info[0].email; // 邮箱
        name.innerHTML = data.info[0].username; // 姓名
        nationality.innerHTML = data.info[0].nationality; // 国籍
        city.innerHTML = data.info[0].city; // 县
        phoneNumber.innerHTML = data.info[0].phone; // 电话号码

    }

    // 请求发送
    request().then((value) => {
        console.log(value);
        render(value);
    });
    // 个人信息
    personalInfo();

}