window.onload = function() {

    alert('密码格式为至少8位数，且必须包含数字，字母和特殊字符');
    // 变量声明

    let flagTest = 0; // 用于指示表单是否通过 通过为0 信息不合格为0 累计全部的数量大小才得以通过

    let withoutName = document.querySelector('.username .without'); // 没有姓名
    let withoutIDCard = document.querySelector('.id-card .without'); // 没有身份证
    let withoutEmail = document.querySelector('.email .without'); // 没有邮箱
    let withoutPassword = document.querySelector('.password .without'); // 没有密码
    let withoutCheckPassword = document.querySelector('.check-password .without'); // 没有确认密码
    let withoutNationality = document.querySelector('.nationality .without'); // 没有国籍
    let withoutPhoneNumber = document.querySelector('.phone-number .without'); // 没有电话号码
    let withoutPostCode = document.querySelector('.postCode .without'); // 没有邮政编码
    let withoutProvince = document.querySelector('.province .without'); // 没有省份
    let withoutCity = document.querySelector('.city .without'); // 没有城市
    let withoutHouseNumber = document.querySelector('.house_number .without'); // 没有门牌号

    let username = document.querySelector('#username'); // 用户名
    let password = document.querySelector('#password'); // 密码
    let checkPassword = document.querySelector('#check-password'); // 密码重复一遍
    let IDCard = document.querySelector('#id-card'); // 身份证
    let email = document.querySelector('#email'); // 邮箱
    let nationality = document.querySelector('#nationality'); // 国籍
    let phoneNumber = document.querySelector('#phone-number'); // 电话号码
    let postCode = document.querySelector('#postCode'); // 邮政编码
    let province = document.querySelector('#province'); // 省份
    let city = document.querySelector('#city'); // 城市
    let houseNumber = document.querySelector('#house_number'); // 门牌号

    let errorIDCard = document.querySelector('.id-card .errorIDCard'); // 错误身份证
    let errorEmail = document.querySelector('.email .errorEmail'); // 错误邮箱
    let errorPassword = document.querySelector('.password .errorPassword'); // 错误密码
    let errorCheckPassword = document.querySelector('.check-password .errorCheckPassword'); // 错误重复密码
    let errorPhone = document.querySelector('.phone-number .errorPhoneNumber'); // 错误电话号码
    let errorPostCode = document.querySelector('.postCode .errorPostCode'); // 错误邮政编码

    let registerBtn = document.querySelector('#register-btn'); // 注册按钮
    let register = document.querySelector('.register'); // 注册框
    let registerSuccess = document.querySelector('.register-success'); // 注册成功

    // 点击注册按钮
    registerBtn.addEventListener('click', function() {
        // flagTest 复位
        flagTest = 0;
        // 姓名为空
        if (username.value == ``) {
            withoutName.style.display = 'block';
            flagTest = 0;
        }
        // 姓名正确
        else {
            withoutName.style.display = 'none';
            flagTest++;
        }
        // 身份证号为空
        if (IDCard.value == ``) {
            errorIDCard.style.display = 'none';
            withoutIDCard.style.display = 'block';
            flagTest = 0;
        }
        // 身份证号错误 
        else if (!checkIDCard(IDCard.value)) {
            withoutIDCard.style.display = 'none';
            errorIDCard.style.display = 'block';
            flagTest = 0;
        }
        // 身份证号正确 
        else {
            withoutIDCard.style.display = 'none';
            errorIDCard.style.display = 'none';
            flagTest++;
        }
        // 密码为空
        if (password.value == ``) {
            errorPassword.style.display = 'none';
            withoutPassword.style.display = 'block';
            flagTest = 0;
        }
        // 密码格式错误
        else if (!isPassword(password.value)) {
            withoutPassword.style.display = 'none';
            errorPassword.style.display = 'block';
            flagTest = 0;
        }
        // 密码正确
        else {
            withoutPassword.style.display = 'none';
            errorPassword.style.display = 'none';
            flagTest++;
        }
        // 确认密码为空
        if (checkPassword.value == ``) {
            withoutCheckPassword.style.display = 'block';
            errorCheckPassword.style.display = 'none';
            flagTest = 0;
        }
        // 确认密码与密码并不相同
        else if (checkPassword.value != password.value) {
            withoutCheckPassword.style.display = 'none';
            errorCheckPassword.style.display = 'block';
            flagTest = 0;
        }
        // 确认密码与密码相同
        else {
            withoutCheckPassword.style.display = 'none';
            errorCheckPassword.style.display = 'none';
            flagTest++;
        }
        // 国籍为空
        if (nationality.value == ``) {
            withoutNationality.style.display = 'block';
            flagTest = 0;
        }
        // 国籍正确
        else {
            withoutNationality.style.display = 'none';
            flagTest++;
        }
        // 电话号码为空
        if (phoneNumber.value == ``) {
            withoutPhoneNumber.style.display = 'block';
            errorPhone.style.display = 'none';
            flagTest = 0;
        }
        // 电话号码错误 
        else if (!isPhoneNumber(phoneNumber.value)) {
            withoutPhoneNumber.style.display = 'none';
            errorPhone.style.display = 'block';
            flagTest = 0;
        }
        // 电话号码正确 
        else {
            withoutPhoneNumber.style.display = 'none';
            errorPhone.style.display = 'none';
            flagTest++;
        }
        // 邮箱为空
        if (email.value == ``) {
            withoutEmail.style.display = 'block';
            errorEmail.style.display = 'none';
            flagTest = 0;
        }
        // 邮箱格式错误
        else if (!isEmail(email.value)) {
            withoutEmail.style.display = 'none';
            errorEmail.style.display = 'block';
            flagTest = 0;
        }
        // 邮箱格式正确
        else {
            withoutEmail.style.display = 'none';
            errorEmail.style.display = 'none';
            flagTest++;
        }
        // 邮政编码为空
        if (postCode.value == ``) {
            withoutPostCode.style.display = 'block';
            errorPostCode.style.display = 'none';
            flagTest = 0;
        }
        // 邮政编码错误 
        else if (!isPostCode(postCode.value)) {
            withoutPostCode.style.display = 'none';
            errorPostCode.style.display = 'block';
            flagTest = 0;
        }
        // 邮政编码正确
        else {
            withoutPostCode.style.display = 'none';
            errorPostCode.style.display = 'none';
            flagTest++;
        }
        // 省份为空
        if (province.value == ``) {
            withoutProvince.style.display = 'block';
            flagTest = 0;
        }
        // 省份正确
        else {
            withoutProvince.style.display = 'none';
            flagTest++;
        }
        // 城市为空
        if (city.value == ``) {
            withoutCity.style.display = 'block';
            flagTest = 0;
        }
        // 城市正确
        else {
            withoutCity.style.display = 'none';
            flagTest++;
        }
        // 门牌号为空
        if (houseNumber.value == ``) {
            withoutHouseNumber.style.display = 'block';
            flagTest = 0;
        }
        // 门牌号正确
        else {
            withoutHouseNumber.style.display = 'none';
            flagTest++;
        }
        // 格式全部正确
        if (flagTest === 11) {

            let data = new Object();
            data.username = `${username.value}`;
            data.IDCard = `${IDCard.value}`;
            data.password = `${password.value}`;
            data.checkPassword = `${checkPassword.value}`;
            data.nationality = `${nationality.value}`;
            data.phoneNumber = `${phoneNumber.value}`;
            data.postCode = `${postCode.value}`;
            data.province = `${province.value}`;
            data.city = `${city.value}`;
            data.houseNumber = `${houseNumber.value}`;
            data.email = `${email.value}`;

            const xhr = new XMLHttpRequest();
            xhr.open('POST', 'http://localhost:8000/register');
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded'); // 后台需要的是json类型的字符串
            xhr.send(JSON.stringify(data));
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200 && xhr.status < 300) {
                        if (JSON.parse(xhr.response).status === 1) {
                            alert('账号注册成功！！！');
                            window.location.href = '../views/login.html';
                        }
                    }
                }
            }
        }
    });
}