window.onload = function() {

    // 请求模块 参数1 对象类型的data 参数2 地址后缀
    function request(data, url) {
        return new Promise((resolve, reject) => {
            // console.log(data);
            const xhr = new XMLHttpRequest();
            xhr.open('POST', `http://localhost:8000${url}`);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded'); // 后台需要的是json类型的字符串
            xhr.send(JSON.stringify(data));
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200 && xhr.status < 300) {
                        let response = JSON.parse(xhr.response);
                        resolve(response);
                    }
                }
            }
        });
    }

    // 专利查看模块
    function checkPatents() {

        // 变量声明
        let mask = document.querySelector('.mask'); // 遮罩层
        let form = document.querySelector('form'); // 表单

        let patentOutlines = document.querySelectorAll('.patentOutline'); //第一次加载时的所有专利概要 
        let image1s = document.querySelectorAll('.image1'); // ‘所有’的专利概要中的照片1
        let image2s = document.querySelectorAll('.image2'); // ‘所有’的专利概要中的照片2
        let image3s = document.querySelectorAll('.image3'); // ‘所有’的专利概要中的照片3
        let patentNames = document.querySelectorAll('.patentName'); // ‘所有’的专利名
        let patentTypes = document.querySelectorAll('.patentType'); // ‘所有’的专利类型
        let patentLabels = document.querySelectorAll('.patentLabel'); // 所有的专利标签
        let introduces = document.querySelectorAll('.introduce'); // 所有的专利介绍
        let patentNumbers = document.querySelectorAll('.patentNumber'); // 所有的专利id

        let patentName = document.querySelector('#patentName'); // 详细的专利名
        let introduce = document.querySelector('#introduce'); // 详细的专利介绍
        let image1ID = document.querySelector('#image1-id'); // 详细的图片1
        let image2ID = document.querySelector('#image2-id'); // 详细的图片2
        let image3ID = document.querySelector('#image3-id'); // 详细的图片3
        let type = document.querySelector('#type'); // 详细的专利类型
        let label = document.querySelector('#label'); // 详细的专利标签

        // 给每一个专利概要注册点击事件
        for (let i = 0; i < patentOutlines.length; i++) {
            patentOutlines[i].onclick = function() {
                // 表单显示
                form.style.display = 'block';
                //遮罩层显示
                mask.style.display = 'block';
                // 将被点击的概要的信息填写到表单上
                patentName.value = patentNames[i].innerHTML;
                introduce.value = introduces[i].innerHTML;
                type.value = patentTypes[i].innerHTML;
                label.value = patentLabels[i].innerHTML;
                image1ID.src = image1s[i].src;
                image2ID.src = image2s[i].src;
                image3ID.src = image3s[i].src;
                clickBtn(patentNumbers[i].innerHTML);
            }
        }
    }

    // 按钮点击
    function clickBtn(id) {
        // 变量声明
        let reject = document.querySelector('#reject'); // 驳回按钮
        let revert = document.querySelector('#revert'); // 返回按钮
        let adopt = document.querySelector('#adopt'); // 通过按钮

        let mask = document.querySelector('.mask'); // 遮罩层
        let form = document.querySelector('form'); // 表单

        // 点击驳回按钮
        reject.onclick = function() {
            // 隐藏遮罩层
            mask.style.display = 'none';
            // 隐藏表单
            form.style.display = 'none';
            let data = new Object();
            let patent = JSON.parse(localStorage.getItem('patent'));
            data.authorization = patent.token;
            data.idCard = patent.idCard;
            data.user = patent.user;
            data.id = id;
            let url = `/reject`;
            request(data, url).then((value) => {
                if (value.status === 0) {
                    let patentOutlines = document.querySelectorAll('.patentOutline'); //第一次加载时的所有专利概要
                    let patentNumbers = document.querySelectorAll('.patentNumber'); // 所有的专利id
                    let patentContainer = document.querySelector('.patent-container');
                    for (let i = 0; i < patentOutlines.length; i++) {
                        if (patentNumbers[i].innerHTML === id) {
                            patentContainer.removeChild(patentOutlines[i]);
                        }
                    }
                }
            });
        }

        // 点击返回按钮
        revert.onclick = function() {
            // 隐藏遮罩层
            mask.style.display = 'none';
            // 隐藏表单
            form.style.display = 'none';
        }

        // 点击通过按钮
        adopt.onclick = function() {
            // console.log(id);
            // 隐藏遮罩层
            mask.style.display = 'none';
            // 隐藏表单
            form.style.display = 'none';
            // 发送请求 请求将该专利加入到approved表 并移除pending
            let data = new Object();
            let patent = JSON.parse(localStorage.getItem('patent'));
            data.authorization = patent.token;
            data.idCard = patent.idCard;
            data.user = patent.user;
            // console.log(id);
            data.id = id;
            // console.log(data.id);
            let url = `/approve`;
            request(data, url).then((value) => {
                if (value.status === 0) {
                    let patentOutlines = document.querySelectorAll('.patentOutline'); //第一次加载时的所有专利概要
                    let patentNumbers = document.querySelectorAll('.patentNumber'); // 所有的专利id
                    let patentContainer = document.querySelector('.patent-container');
                    for (let i = 0; i < patentOutlines.length; i++) {
                        if (patentNumbers[i].innerHTML === id) {
                            patentContainer.removeChild(patentOutlines[i]);
                        }
                    }
                }
            });
        }
    }

    // 渲染页面
    function render(data) {
        // 专利信息加载 函数
        function patentLoading(module, i) {
            // 变量声明
            let patentOutlines = document.querySelectorAll(`.${module} .patentOutline`); //第一次加载时的所有专利概要 
            let patentContainer = document.querySelector(`.${module} .patent-container`); // 展示所有专利信息的地方
            // 克隆模板专利概要
            let patentOutline = patentOutlines[0].cloneNode(true);
            patentOutline.style.display = 'flex';
            // 从后面开始获取信息 将每一个信息都加载到模板的下面
            if (patentOutlines.length === 1) {
                patentContainer.appendChild(patentOutline);
            } else {
                patentContainer.insertBefore(patentOutline, patentOutlines[1]);
            }
        }

        // 审核中专利
        for (let i = 0; i < data.pending.length; i++) {
            let module = `needPass`;
            patentLoading(module, i);
            // 信息填充
            let image1s = document.querySelectorAll(`.${module} .image1`); // ‘所有’的专利概要中的照片1
            let image2s = document.querySelectorAll(`.${module} .image2`); // ‘所有’的专利概要中的照片2
            let image3s = document.querySelectorAll(`.${module} .image3`); // ‘所有’的专利概要中的照片3
            let patentNames = document.querySelectorAll(`.${module} .patentName`); // ‘所有’的专利名
            let patentTypes = document.querySelectorAll(`.${module} .patentType`); // ‘所有’的专利类型
            let patentLabels = document.querySelectorAll(`.${module} .patentLabel`); // 所有的专利标签
            let introduces = document.querySelectorAll(`.${module} .introduce`); // 所有的专利介绍
            let patentNumbers = document.querySelectorAll(`.${module} .patentNumber`); // 申请中专利的唯一标识

            let fileReader = new FileReader();
            fileReader.onload = () => {
                console.log(fileReader.result);
            };
            fileReader.readAsDataURL(data.pending[i].image_1);
            image1s[1].src = data.pending[i].image_1;
            image2s[1].src = data.pending[i].image_2;
            image3s[1].src = data.pending[i].image_3;
            patentNames[1].innerHTML = data.pending[i].name;
            patentTypes[1].innerHTML = data.pending[i].type;
            patentLabels[1].innerHTML = data.pending[i].label;
            introduces[1].innerHTML = data.pending[i].introduce;
            patentNumbers[1].innerHTML = data.pending[i].id;
        }

        // 这个给所有专利注册点击事件的函数放里面是可以的 放外面会获取节点错误
        checkPatents();

    }

    // 请求发送
    let data = new Object();
    let patent = JSON.parse(localStorage.getItem('patent'));
    data.authorization = patent.token;
    data.idCard = patent.idCard;
    data.user = patent.user;
    let url = `/pending`;
    request(data, url).then((value) => {
        // console.log(value);
        // 渲染页面并且给专利按钮注册点击事件
        render(value);
    });

}