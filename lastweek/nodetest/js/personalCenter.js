window.onload = function() {

    // 个人信息模块
    function personalInfo() {
        // 变量声明
        let infoReviseBtn = document.querySelector('.personal-container .reviseBtn'); // 个人信息的修改按钮
        let infoSaveBtn = document.querySelector('.personal-container .saveBtn'); // 个人信息的保存按钮
        let mask = document.querySelector('.mask'); // 个人资料表面的遮罩层
        let infoInputs = document.querySelectorAll('.personal-container input'); // 个人资料的修改框 邮箱地址 电话号码

        // 点击个人信息的修改按钮  
        infoReviseBtn.addEventListener('click', function() {
            // 透明遮罩层消失
            mask.style.display = 'none';
            // 输入框出现
            for (let i = 0; i < infoInputs.length; i++) {
                infoInputs[i].style.border = '1px solid #666';
            }
            // 修改按钮隐藏
            infoReviseBtn.style.display = 'none';
            // 保存按钮显示
            infoSaveBtn.style.display = 'block';
        });

        // 点击个人信息的保存按钮
        infoSaveBtn.addEventListener('click', function() {
            // 透明遮罩层显示
            mask.style.display = 'block';
            // 输入框消失
            for (let i = 0; i < infoInputs.length; i++) {
                infoInputs[i].style.border = 'none';
            }
            // 修改按钮显示
            infoReviseBtn.style.display = 'block';
            // 保存按钮隐藏
            infoSaveBtn.style.display = 'none';
        });
    }

    // 点击小导航切换对应到对应的模块
    function change() {

        let navs = document.querySelectorAll('.nav div a'); // 导航栏中的五个小导航
        let articles = document.querySelectorAll('.article>div'); // 文章中的五个小模块

        for (let i = 0; i < navs.length; i++) {
            navs[i].addEventListener('click', function() {
                for (let i = 0; i < navs.length; i++) {
                    // 清除所有小导航的下边框
                    navs[i].style.borderBottom = 'none';
                    // 隐藏所有的模块
                    articles[i].style.display = 'none';
                }
                // 为自己加上下边框
                this.style.borderBottom = '1px solid #666';
                // 显示对应模块
                articles[i].style.display = 'flex';
                // 对应‘已驳回’模块 跟‘未提交’模块时 删除按钮显示 其余隐藏
                abandon.style.display = 'none';
                if (i === 1 || i === 3) {
                    abandon.style.display = 'block';
                }
            });
        }
    }

    // 专利查看模块
    function checkPatents() {
        // 变量声明
        let revert = document.querySelector('#revert'); // 返回按钮
        var abandon = document.querySelector('#abandon'); // 删除按钮

        let bigMask = document.querySelector('#mask'); // 遮罩层
        let form = document.querySelector('form'); // 表单

        let patentOutlines = document.querySelectorAll('.patentOutline'); //第一次加载时的所有专利概要 
        let image1s = document.querySelectorAll('.image1'); // ‘所有’的专利概要中的照片1
        let image2s = document.querySelectorAll('.image2'); // ‘所有’的专利概要中的照片2
        let image3s = document.querySelectorAll('.image3'); // ‘所有’的专利概要中的照片3
        let patentNames = document.querySelectorAll('.patentName'); // ‘所有’的专利名
        let patentTypes = document.querySelectorAll('.patentType'); // ‘所有’的专利类型
        let patentLabels = document.querySelectorAll('.patentLabel'); // 所有的专利标签
        let introduces = document.querySelectorAll('.introduce'); // 所有的专利介绍

        let patentName = document.querySelector('#patentName'); // 详细的专利名
        let introduce = document.querySelector('#introduce'); // 详细的专利介绍
        let image1ID = document.querySelector('#image1-id'); // 详细的图片1
        let image2ID = document.querySelector('#image2-id'); // 详细的图片2
        let image3ID = document.querySelector('#image3-id'); // 详细的图片3
        let type = document.querySelector('#type'); // 详细的专利类型
        let label = document.querySelector('#label'); // 详细的专利标签

        // 点击返回按钮
        revert.addEventListener('click', function() {
            // 隐藏遮罩层
            bigMask.style.display = 'none';
            // 隐藏表单
            form.style.display = 'none';
        });

        // 点击删除按钮
        abandon.addEventListener('click', function() {
            // 隐藏遮罩层
            bigMask.style.display = 'none';
            // 隐藏表单
            form.style.display = 'none';
        });

        // 给每一个‘每一个’专利概要注册点击事件
        for (let i = 0; i < patentOutlines.length; i++) {
            patentOutlines[i].onclick = function() {
                // 表单显示
                form.style.display = 'block';
                //遮罩层显示
                bigMask.style.display = 'block';
                // 将被点击的概要的信息填写到表单上
                patentName.value = patentNames[i].innerHTML;
                introduce.value = introduces[i].innerHTML;
                type.value = patentTypes[i].innerHTML;
                label.value = patentLabels[i].innerHTML;
                image1ID.src = image1s[i].src;
                image2ID.src = image2s[i].src;
                image3ID.src = image3s[i].src;
            };
        }
    }

    // 发送请求返回用户数据
    function request() {
        return new Promise((resolve, reject) => {
            let data = new Object();
            let patent = JSON.parse(localStorage.getItem('patent'));
            data.authorization = patent.token;
            data.idCard = patent.idCard;
            data.user = patent.user;
            // console.log(data);
            const xhr = new XMLHttpRequest();
            xhr.open('POST', 'http://localhost:8000/personal');
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded'); // 后台需要的是json类型的字符串
            xhr.send(JSON.stringify(data));
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200 && xhr.status < 300) {
                        let response = JSON.parse(xhr.response);
                        resolve(response);
                    }
                }
            }
        });
    }

    // 用户数据渲染页面
    function render(data) {

        // 个人信息
        let account = document.querySelector('.account>p'); // 账户
        let postCode = document.querySelector('.postCode>p'); // 邮政编码
        let address = document.querySelector('.address>p'); // 详细地址
        let indetityNumber = document.querySelector('.identityNumber>p'); // 证件号码
        let province = document.querySelector('.province>p'); // 省份
        let email = document.querySelector('.email>input'); // 邮箱
        let name = document.querySelector('.name>p'); // 姓名
        let nationality = document.querySelector('.nationality>p'); // 国籍
        let city = document.querySelector('.city>p'); // 县
        let phoneNumber = document.querySelector('.phoneNumber>input'); // 电话号码

        account.innerHTML = data.info[0].idCard; // 账户
        postCode.innerHTML = data.info[0].postCode; // 邮编
        address.innerHTML = data.info[0].house_number; // 详细地址
        indetityNumber.innerHTML = data.info[0].idCard; // 证件号码
        province.innerHTML = data.info[0].province; // 省份
        email.innerHTML = data.info[0].email; // 邮箱
        name.innerHTML = data.info[0].username; // 姓名
        nationality.innerHTML = data.info[0].nationality; // 国籍
        city.innerHTML = data.info[0].city; // 县
        phoneNumber.innerHTML = data.info[0].phone; // 电话号码

        // 专利信息加载 函数
        function patentLoading(module, i) {

            // 变量声明
            let patentOutlines = document.querySelectorAll(`.${module} .patentOutline`); //第一次加载时的所有专利概要 
            let patentContainer = document.querySelector(`.${module} .patent-container`); // 展示所有专利信息的地方
            // 克隆模板专利概要
            let patentOutline = patentOutlines[0].cloneNode(true);
            patentOutline.style.display = 'flex';
            // 从后面开始获取信息 将每一个信息都加载到模板的下面
            if (patentOutlines.length === 1) {
                patentContainer.appendChild(patentOutline);
            } else {
                patentContainer.insertBefore(patentOutline, patentOutlines[1]);
            }
        }

        // 已通过专利
        for (let i = 0; i < data.approved.length; i++) {
            let module = `passed`;
            patentLoading(module, i);
            // 信息填充
            let image1s = document.querySelectorAll(`.${module} .image1`); // ‘所有’的专利概要中的照片1
            let image2s = document.querySelectorAll(`.${module} .image2`); // ‘所有’的专利概要中的照片2
            let image3s = document.querySelectorAll(`.${module} .image3`); // ‘所有’的专利概要中的照片3
            let patentNames = document.querySelectorAll(`.${module} .patentName`); // ‘所有’的专利名
            let patentTypes = document.querySelectorAll(`.${module} .patentType`); // ‘所有’的专利类型
            let patentLabels = document.querySelectorAll(`.${module} .patentLabel`); // 所有的专利标签
            let introduces = document.querySelectorAll(`.${module} .introduce`); // 所有的专利介绍

            image1s[1].src = data.approved[i].image_1;
            image2s[1].src = data.approved[i].image_2;
            image3s[1].src = data.approved[i].image_3;
            patentNames[1].innerHTML = data.approved[i].name;
            patentTypes[1].innerHTML = data.approved[i].type;
            patentLabels[1].innerHTML = data.approved[i].label;
            introduces[1].innerHTML = data.approved[i].introduce;
        }

        // 已驳回专利
        for (let i = 0; i < data.reject.length; i++) {
            let module = `reject`;
            patentLoading(module, i);
            // 信息填充
            let image1s = document.querySelectorAll(`.${module} .image1`); // ‘所有’的专利概要中的照片1
            let image2s = document.querySelectorAll(`.${module} .image2`); // ‘所有’的专利概要中的照片2
            let image3s = document.querySelectorAll(`.${module} .image3`); // ‘所有’的专利概要中的照片3
            let patentNames = document.querySelectorAll(`.${module} .patentName`); // ‘所有’的专利名
            let patentTypes = document.querySelectorAll(`.${module} .patentType`); // ‘所有’的专利类型
            let patentLabels = document.querySelectorAll(`.${module} .patentLabel`); // 所有的专利标签
            let introduces = document.querySelectorAll(`.${module} .introduce`); // 所有的专利介绍

            image1s[1].src = data.reject[i].image_1;
            image2s[1].src = data.reject[i].image_2;
            image3s[1].src = data.reject[i].image_3;
            patentNames[1].innerHTML = data.reject[i].name;
            patentTypes[1].innerHTML = data.reject[i].type;
            patentLabels[1].innerHTML = data.reject[i].label;
            introduces[1].innerHTML = data.reject[i].introduce;
        }

        // 审核中专利
        for (let i = 0; i < data.pending.length; i++) {
            let module = `audit`;
            patentLoading(module, i);
            // 信息填充
            let image1s = document.querySelectorAll(`.${module} .image1`); // ‘所有’的专利概要中的照片1
            let image2s = document.querySelectorAll(`.${module} .image2`); // ‘所有’的专利概要中的照片2
            let image3s = document.querySelectorAll(`.${module} .image3`); // ‘所有’的专利概要中的照片3
            let patentNames = document.querySelectorAll(`.${module} .patentName`); // ‘所有’的专利名
            let patentTypes = document.querySelectorAll(`.${module} .patentType`); // ‘所有’的专利类型
            let patentLabels = document.querySelectorAll(`.${module} .patentLabel`); // 所有的专利标签
            let introduces = document.querySelectorAll(`.${module} .introduce`); // 所有的专利介绍

            image1s[1].src = data.pending[i].image_1;
            image2s[1].src = data.pending[i].image_2;
            image3s[1].src = data.pending[i].image_3;
            patentNames[1].innerHTML = data.pending[i].name;
            patentTypes[1].innerHTML = data.pending[i].type;
            patentLabels[1].innerHTML = data.pending[i].label;
            introduces[1].innerHTML = data.pending[i].introduce;
        }

        // 未提交专利
        for (let i = 0; i < data.editing.length; i++) {
            let module = `uncommitted`;
            patentLoading(module, i);
            // 信息填充
            let image1s = document.querySelectorAll(`.${module} .image1`); // ‘所有’的专利概要中的照片1
            let image2s = document.querySelectorAll(`.${module} .image2`); // ‘所有’的专利概要中的照片2
            let image3s = document.querySelectorAll(`.${module} .image3`); // ‘所有’的专利概要中的照片3
            let patentNames = document.querySelectorAll(`.${module} .patentName`); // ‘所有’的专利名
            let patentTypes = document.querySelectorAll(`.${module} .patentType`); // ‘所有’的专利类型
            let patentLabels = document.querySelectorAll(`.${module} .patentLabel`); // 所有的专利标签
            let introduces = document.querySelectorAll(`.${module} .introduce`); // 所有的专利介绍

            image1s[1].src = data.editing[i].image_1;
            image2s[1].src = data.editing[i].image_2;
            image3s[1].src = data.editing[i].image_3;
            patentNames[1].innerHTML = data.editing[i].name;
            patentTypes[1].innerHTML = data.editing[i].type;
            patentLabels[1].innerHTML = data.editing[i].label;
            introduces[1].innerHTML = data.editing[i].introduce;
        }

    }

    // 请求发送
    request().then((value) => {
        console.log(value);
        render(value);
    });
    // 个人信息
    personalInfo();
    // 导航切换
    change();
    // 专利查看
    checkPatents();

}