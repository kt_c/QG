window.onload = function() {

    // 渲染页面
    function render(data) {

        // 信息加载 函数
        function feedbackLoading(module, i) {

            // 变量声明
            let feedbackOutlines = document.querySelectorAll(`.${module} .feedbackOutline`); //第一次加载时的所有专利概要 
            let feedbackContainer = document.querySelector(`.${module} .feedback-container`); // 展示所有专利信息的地方
            // 克隆模板专利概要
            let feedbackOutline = feedbackOutlines[0].cloneNode(true);
            feedbackOutline.style.display = 'flex';
            // 从后面开始获取信息 将每一个信息都加载到模板的下面
            if (feedbackOutlines.length === 1) {
                feedbackContainer.appendChild(feedbackOutline);
            } else {
                feedbackContainer.insertBefore(feedbackOutline, feedbackOutlines[1]);
            }
        }

        // 反馈信息
        for (let i = 0; i < data.approved.length; i++) {
            let module = `passed`;
            feedbackLoading(module, i);
            // 信息填充
            let sender = document.querySelectorAll(`.${module} .sender`); // 发件人
            let sendeTime = document.querySelectorAll(`.${module} .sendeTime`); // 发件时间
            let sendeContent = document.querySelectorAll(`.${module} .sendeContent`); // 发件内容
            let status = document.querySelectorAll(`.${module} .status`); // 状态

            image1s[1].src = data.approved[i].image_1;
            image2s[1].src = data.approved[i].image_2;
            image3s[1].src = data.approved[i].image_3;
            patentNames[1].innerHTML = data.approved[i].name;
            patentTypes[1].innerHTML = data.approved[i].type;
            patentLabels[1].innerHTML = data.approved[i].label;
            introduces[1].innerHTML = data.approved[i].introduce;
        }
    }

    // 请求获取信息
    let url = `/getFeedbackAndReport`;
    let data = new Object();
    let patent = JSON.parse(localStorage.getItem('patent'));
    data.authorization = patent.token;
    data.idCard = patent.idCard;
    data.user = patent.user;
    request(data, url).then((value) => {
        console.log(value);
        render(value);
    });

    // 变量声明
    let toFeedback = document.querySelector('.nav .toFeedback'); // 信息反馈模块小导航
    let toReport = document.querySelector('.nav .toReport'); // 审核专利举报模块小导航
    let AtoFeedback = document.querySelector('.nav .toFeedback a'); // 信息模块小导航里的链接
    let AtoReport = document.querySelector('.nav .toReport a'); // 举报模块小导航里的链接
    let feedback = document.querySelector('.feedback'); // 审核信息反馈模块
    let report = document.querySelector('.report'); // 审核专利举报模块

    let feedbackOutlines = document.querySelectorAll('.feedbackOutline'); // 页面第一次加载时的所有反馈信息概要
    let senders = document.querySelectorAll('.sender'); // 所有的发件人
    let sendTimes = document.querySelectorAll('.sendTime'); // 所有的发件时间
    let sendContents = document.querySelectorAll('.sendContent'); // 所有的发件内容
    let feedbackStatus = document.querySelectorAll('.feedbackOutline .status'); // 所有的反馈信件的查看状态

    let detailedFeedback = document.querySelector('.detailFeedback'); // 详细信息模块
    let checkCloses = document.querySelectorAll('.checkClose'); // 关闭详细模块的按钮 有两个
    let detailSender = document.querySelector('.detailSender'); // 详细信息中的发件人
    let detailSendTime = document.querySelector('.detailSendTime'); // 详细信息中的发件时间
    let detailTextarea = document.querySelector('.detailFeedback textarea'); // 详细信息中的文本域
    let checked = document.querySelector('#checked'); // 已查看 按钮 反馈

    let reportOutlines = document.querySelectorAll('.reportOutline'); // 页面第一次加载时的所有举报信息
    let reporters = document.querySelectorAll('.reporter'); // 所有举报者
    let reportTimes = document.querySelectorAll('.reportTime'); // 所有的举报时间
    let reportPatents = document.querySelectorAll('.reportPatent'); // 所有的被举报专利号
    let reportContents = document.querySelectorAll('.reportContent'); // 所有的举报内容
    let reportStatuses = document.querySelectorAll('.report .status'); // 所有的举报信件查看状态

    let detailReport = document.querySelector('.detailReport'); // 详细举报模块
    let detailReporter = document.querySelector('.detailReporter'); // 详细举报中的举报者
    let detailReportTime = document.querySelector('.detailReportTime'); // 详细举报中的举报时间
    let detailReportPatent = document.querySelector('.detailReportPatent'); // 详细举报中的被举报专利
    let detailReportTextarea = document.querySelector('.detailReport textarea'); // 详细举报中的文本域
    let reportChecked = document.querySelector('.checked'); // 已查看 按钮 举报

    // console.log(`${detailReporter}`);
    // console.log(`${reporters.length}`);


    // 给‘每一个’反馈概要注册事件
    for (let i = 0; i < feedbackOutlines.length; i++) {
        feedbackOutlines[i].addEventListener('click', function() {
            // 显示详细反馈信息模块
            detailedFeedback.style.display = 'block';
            // 将反馈的信息书写到详细信息文本框上
            detailSender.innerHTML = senders[i].innerHTML;
            detailSendTime.innerHTML = sendTimes[i].innerHTML;
            // 文本域 有没有办法解决首行缩进和换行的问题
            detailTextarea.innerHTML = sendContents[i].innerHTML;
            // 在每一个详细信息里面注册一个‘已查看’按钮的功能 用onclick 保证只注册一次
            checked.onclick = function() {
                feedbackStatus[i].innerHTML = '已查看';
                // 隐藏详细信息模块
                detailedFeedback.style.display = 'none';
            }
        });
    }

    // 点击反馈关闭按钮
    checkCloses[0].addEventListener('click', function() {
        // 隐藏详细信息模块
        detailedFeedback.style.display = 'none';
    });


    // 给‘每一个’举报概要注册事件
    for (let i = 0; i < reportOutlines.length; i++) {
        reportOutlines[i].addEventListener('click', function() {
            // 显示详细举报信息模块
            detailReport.style.display = 'block';
            // 将举报的信息书写到详细信息文本框上
            detailReporter.innerHTML = reporters[i].innerHTML;
            detailReportTime.innerHTML = reportTimes[i].innerHTML;
            detailReportPatent.innerHTML = reportPatents[i].innerHTML;
            // 文本域 有没有办法解决首行缩进和换行的问题
            detailReportTextarea.innerHTML = reportContents[i].innerHTML;
            // 在每一个详细信息里面注册一个‘已查看’按钮的功能 用onclick 保证只注册一次
            reportChecked.onclick = function() {
                reportStatuses[i].innerHTML = '已查看';
                // 隐藏详细信息模块
                detailReport.style.display = 'none';
            }
        });
    }

    // 点击举报关闭按钮
    checkCloses[1].addEventListener('click', function() {
        // 隐藏详细信息模块
        detailReport.style.display = 'none';
    });


    // 点击‘信息’切换按钮
    toFeedback.addEventListener('click', function() {
        // 举报小导航下边框隐藏
        AtoReport.style.borderBottom = 'none';
        // 举报模块隐藏
        report.style.display = 'none';
        // 信息小导航下边框显示
        AtoFeedback.style.borderBottom = '1px solid #000';
        // 信息模块显示
        feedback.style.display = 'flex';
    });

    // 点击‘举报’切换按钮
    toReport.addEventListener('click', function() {
        // // 信息小导航下边框隐藏
        AtoFeedback.style.borderBottom = 'none';
        // 信息模块隐藏
        feedback.style.display = 'none';
        // 举报小导航下边框显示
        AtoReport.style.borderBottom = '1px solid #000';
        // 举报模块显示
        report.style.display = 'flex';
    });
}