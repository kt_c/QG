window.addEventListener('load', function() {

    let inputBox = document.querySelector('section input[type="search"]'); // 搜索框
    let searchBtn = document.querySelector('section button'); // 搜索按钮
    let checkboxes = document.querySelectorAll('section input[type="checkbox"]'); // section里面的所有复选框

    // 函数 渲染页面 参数是对象类型数据
    function render(data) {
        console.log(data);

        // 专利信息加载 函数
        function patentLoading(module, i) {

            // 变量声明
            let patentOutlines = document.querySelectorAll(`.${module} .patentOutline`); //第一次加载时的所有专利概要 
            let patentContainer = document.querySelector(`.${module} .patent-container`); // 展示所有专利信息的地方
            // 克隆模板专利概要
            let patentOutline = patentOutlines[0].cloneNode(true);
            patentOutline.style.display = 'flex';
            // 从后面开始获取信息 将每一个信息都加载到模板的下面
            if (patentOutlines.length === 1) {
                patentContainer.appendChild(patentOutline);
            } else {
                patentContainer.insertBefore(patentOutline, patentOutlines[1]);
            }
        }

        // 已通过专利
        for (let i = 0; i < data.approved.length; i++) {
            let module = `patent`;
            patentLoading(module, i);
            // 信息填充
            let image1s = document.querySelectorAll(`.${module} .image1`); // ‘所有’的专利概要中的照片1
            let image2s = document.querySelectorAll(`.${module} .image2`); // ‘所有’的专利概要中的照片2
            let image3s = document.querySelectorAll(`.${module} .image3`); // ‘所有’的专利概要中的照片3
            let patentNames = document.querySelectorAll(`.${module} .patentName`); // ‘所有’的专利名
            let patentTypes = document.querySelectorAll(`.${module} .patentType`); // ‘所有’的专利类型
            let patentLabels = document.querySelectorAll(`.${module} .patentLabel`); // 所有的专利标签
            let introduces = document.querySelectorAll(`.${module} .introduce`); // 所有的专利介绍

            image1s[1].src = data.approved[i].image1;
            image2s[1].src = data.approved[i].image2;
            image3s[1].src = data.approved[i].image3;
            patentNames[1].innerHTML = data.approved[i].name;
            patentTypes[1].innerHTML = data.approved[i].type;
            patentLabels[1].innerHTML = data.approved[i].label;
            introduces[1].innerHTML = data.approved[i].introduce;
        }
    }

    // 专利查看模块
    function checkPatents() {
        // 变量声明
        let revert = document.querySelector('#revert'); // 返回按钮

        let bigMask = document.querySelector('#mask'); // 遮罩层
        let form = document.querySelector('form'); // 表单

        let patentOutlines = document.querySelectorAll('.patentOutline'); //第一次加载时的所有专利概要 
        let image1s = document.querySelectorAll('.image1'); // ‘所有’的专利概要中的照片1
        let image2s = document.querySelectorAll('.image2'); // ‘所有’的专利概要中的照片2
        let image3s = document.querySelectorAll('.image3'); // ‘所有’的专利概要中的照片3
        let patentNames = document.querySelectorAll('.patentName'); // ‘所有’的专利名
        let patentTypes = document.querySelectorAll('.patentType'); // ‘所有’的专利类型
        let patentLabels = document.querySelectorAll('.patentLabel'); // 所有的专利标签
        let introduces = document.querySelectorAll('.introduce'); // 所有的专利介绍

        let patentName = document.querySelector('#patentName'); // 详细的专利名
        let introduce = document.querySelector('#introduce'); // 详细的专利介绍
        let image1ID = document.querySelector('#image1-id'); // 详细的图片1
        let image2ID = document.querySelector('#image2-id'); // 详细的图片2
        let image3ID = document.querySelector('#image3-id'); // 详细的图片3
        let type = document.querySelector('#type'); // 详细的专利类型
        let label = document.querySelector('#label'); // 详细的专利标签

        // 点击返回按钮
        revert.addEventListener('click', function() {
            // 隐藏遮罩层
            bigMask.style.display = 'none';
            // 隐藏表单
            form.style.display = 'none';
        });

        // 给每一个‘每一个’专利概要注册点击事件
        for (let i = 0; i < patentOutlines.length; i++) {
            patentOutlines[i].onclick = function() {
                // 表单显示
                form.style.display = 'block';
                //遮罩层显示
                bigMask.style.display = 'block';
                // 将被点击的概要的信息填写到表单上
                patentName.value = patentNames[i].innerHTML;
                introduce.value = introduces[i].innerHTML;
                type.value = patentTypes[i].innerHTML;
                label.value = patentLabels[i].innerHTML;
                image1ID.src = image1s[i].src;
                image2ID.src = image2s[i].src;
                image3ID.src = image3s[i].src;
            };
        }
    }

    // 清除页面数据
    function clear() {
        let patentContainer = document.querySelector('.patent-container'); // 专利概要的容器
        let patentOutlines = document.querySelectorAll('.patentOutline'); // 页面现有的所有专利概要
        for (let i = 1; i < patentOutlines.length; i++) {
            patentContainer.removeChild(patentOutlines[i]);
        }
    }

    // 点击搜索按钮
    searchBtn.addEventListener('click', function() {
        // 这里就只做一个简单的搜索功能 对用户输入的内容完完全全地进行搜索 不进行匹配
        let url = `/search`;
        let data = new Object();
        let patent = JSON.parse(localStorage.getItem('patent'));
        data.authorization = patent.token;
        data.idCard = patent.idCard;
        data.user = patent.user;
        data.search = {
            value: inputBox.value,
            invention: 1,
            utility: 1,
            appearance: 1,
        };
        if (checkboxes[0].checked) {
            data.search.invention = 0;
        }
        if (checkboxes[1].checked) {
            data.search.utility = 0;
        }
        if (checkboxes[2].checked) {
            data.search.appearance = 0;
        }
        // 清除页面现有数据
        clear();
        // 想服务器请求数据
        request(data, url).then((value) => {
            if (value) {
                render(value);
                checkPatents();
            }
        });
    });
});