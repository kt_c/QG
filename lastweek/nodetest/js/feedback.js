window.onload = function() {

    // 变量声明
    let toFeedback = document.querySelector('.toFeedback'); // 切换至反馈单
    let toReport = document.querySelector('.toReport'); // 切换至举报单
    let feedback = document.querySelector('.feedback'); // 反馈模块
    let report = document.querySelector('.report'); // 举报模块
    let feedbackBtn = document.querySelector('.feedback button'); // 反馈提交按钮
    let reportBtn = document.querySelector('.report button'); // 举报的提交按钮

    // 点击toFeedback
    toFeedback.addEventListener('click', function() {
        toReport.style.backgroundColor = 'transparent';
        toFeedback.style.backgroundColor = 'brown';
        feedback.style.display = 'flex';
        report.style.display = 'none';
    });

    // 点击toReport
    toReport.addEventListener('click', function() {
        toFeedback.style.backgroundColor = 'transparent';
        toReport.style.backgroundColor = 'brown';
        report.style.display = 'flex';
        feedback.style.display = 'none';
    });

    // 点击feedbackBtn
    feedbackBtn.addEventListener('click', function() {
        let feedbackText = document.querySelector('#feedbackText'); // 反馈的内容
        let url = `/feedback`;
        let data = new Object();
        let patent = JSON.parse(localStorage.getItem('patent'));
        data.authorization = patent.token;
        data.idCard = patent.idCard;
        data.user = patent.user;
        data.text = feedbackText.value;
        request(data, url).then((value) => {
            if (value.status === 0) {
                alert('反馈成功！！！');
                feedbackText.value = '';
            }
        });
    });

    // 点击reportBtn
    reportBtn.addEventListener('click', function() {
        let reportText = document.querySelector('#reportText'); // 举报的内容
        let reportID = document.querySelector('#reportID'); // 被举报的专利号
        let url = `/report`;
        let data = new Object();
        let patent = JSON.parse(localStorage.getItem('patent'));
        data.authorization = patent.token;
        data.idCard = patent.idCard;
        data.user = patent.user;
        data.text = reportText.value;
        data.reportID = reportID.value;
        request(data, url).then((value) => {
            if (value.status === 0) {
                alert(`${value.message}`);
                reportText.value = '';
                reportID.value = '';
            }
        });
    });
}