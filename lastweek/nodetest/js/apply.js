window.addEventListener('load', function() {

    // 变量声明
    let revertBtn = document.querySelector('.btn button:nth-child(1)'); // 返回按钮
    let setBtn = document.querySelector('.btn button:nth-child(2)') // 保存按钮
    let submitBtn = document.querySelector('.btn button:nth-child(3)'); // 提交按钮
    let mask = document.querySelector('.mask'); // 遮罩层

    let flagTest = 0; // 用于指示表单是否通过 通过为0 信息不合格为1
    let flagSet = 0; // 用于指示表单是否保存 已保存自增 未保存为0

    let inventor = document.querySelector('#inventor'); // 发明人姓名
    let inventorIDCard = document.querySelector('#inventorIDCard'); // 发明人身份证号
    let patentName = document.querySelector('#patentName'); // 专利名
    let introduce = document.querySelector('#introduce'); // 专利介绍
    let type = document.querySelector('#type'); // 专利类型
    let label = document.querySelector('#label'); // 专利标签
    let files = document.querySelectorAll('input[type="file"]'); // 获取所有file类型的input
    let images = document.querySelectorAll(`.image-id`);

    let withoutInventor = document.querySelector('.inventor .without'); // 没有发明人姓名
    let withoutInventorIDCard = document.querySelector('.inventorIDCard .without'); // 没有发明人身份证号
    let withoutPatentName = document.querySelector('.patentName .without'); // 没有专利名
    let withoutIntroduce = document.querySelector('.introduce .without'); // 没有专利介绍
    let withoutLabel = document.querySelector('.label .without'); // 没有专利标签

    let errorIDCard = document.querySelector('.inventorIDCard .errorIDCard'); // 错误身份证号提示

    let image_1;
    let image_2;
    let image_3;

    function isNullImage() {
        return new Promise((resolve) => {
            for (let i = 0; i < images.length; i++) {
                if (images[i].src = ``) {
                    resolve(true);
                }
            }
        });
    }

    function showImage() {
        // 这个功能暂时有问题 用于处理图片      
        for (let i = 0; i < files.length; i++) {
            files[i].addEventListener('change', function(file) {
                let filesArr = file.target.files;
                let fileReader = new FileReader();
                fileReader.onload = () => {
                    // 图片展示
                    images[i].src = fileReader.result;
                };
                fileReader.readAsDataURL(filesArr[0]);
            });
        }
    }

    function putImage() {
        files[0].addEventListener('change', function(file) {
            let filesArr = file.target.files;
            let fileReader = new FileReader();
            fileReader.onload = () => {
                image_1 = fileReader.result;
            };
            fileReader.readAsDataURL(filesArr[0]);
        });
        files[1].addEventListener('change', function(file) {
            let filesArr = file.target.files;
            let fileReader = new FileReader();
            fileReader.onload = () => {
                image_2 = fileReader.result;
            };
            fileReader.readAsDataURL(filesArr[0]);
        });
        files[2].addEventListener('change', function(file) {
            let filesArr = file.target.files;
            let fileReader = new FileReader();
            fileReader.onload = () => {
                image_3 = fileReader.result;
            };
            fileReader.readAsDataURL(filesArr[0]);
        });
    }

    showImage();
    putImage();

    // 表单验证 点击提交按钮
    submitBtn.addEventListener('click', function() {
        // 发明者姓名为空
        if (inventor.value == ``) {
            withoutInventor.style.display = 'block';
            flagTest = 0;
        }
        // 发明者姓名正确
        else {
            withoutInventor.style.display = 'none';
            flagTest++;
        }
        // 发明者身份号为空
        if (inventorIDCard.value == ``) {
            withoutInventorIDCard.style.display = 'block';
            errorIDCard.style.display = 'none';
            flagTest = 0;
        }
        // 错误身份证号 
        else if (!checkIDCard(inventorIDCard.value)) {
            withoutInventor.style.display = 'none';
            errorIDCard.style.display = 'block';
            flagTest = 0;
        }
        // 发明者身份号正确
        else {
            withoutInventor.style.display = 'none';
            flagTest++;
        }
        // 专利名为空
        if (patentName.value == ``) {
            withoutPatentName.style.display = 'block';
            flagTest++;
        }
        // 专利名正确
        else {
            withoutPatentName.style.display = 'none';
            flagTest++;
        }
        // 专利介绍为空
        if (introduce.value == ``) {
            withoutIntroduce.style.display = 'block';
            flagTest = 0;
        }
        // 专利介绍正确
        else {
            withoutIntroduce.style.display = 'none';
            flagTest++;
        }
        // 专利标签为空
        if (label.value == ``) {
            withoutLabel.style.display = 'block';
            flagTest = 0;
        }
        // 专利标签正确
        else {
            withoutLabel.style.display = 'none';
            flagTest++;
        }
        if (flagTest === 5) {
            let url = `/apply`;
            // 数据自定义
            let data = new Object();
            let patent = JSON.parse(localStorage.getItem('patent'));
            data.authorization = patent.token;
            data.idCard = patent.idCard;
            data.user = patent.user;
            data.inventor = {
                name: inventor.value,
                idCard: inventorIDCard.value,
            }
            data.apply = {
                inventor: inventorIDCard.value,
                name: patentName.value,
                introduce: introduce.value,
                label: label.value,
                type: type.value,
                image_1: image_1,
                image_2: image_2,
                image_3: image_3,
            };
            // console.log(data.apply.image_1);
            request(data, url).then((value) => {
                if (value.status === 1) {
                    window.location = '../views/index.html';
                    alert('专利申请提交成功!!');
                } else {
                    alert('提交失败!!!');
                }
            });

        }
    });

    // 点击保存按钮
    setBtn.addEventListener('click', function() {
        // 保存状态改变
        flagSet = 0;
    });

    // 点击返回按钮
    revertBtn.addEventListener('click', function() {
        if (flagSet === 0) {
            window.location = '../views/index.html';
        } else {
            checkSet.style.display = 'flex';
            mask.style.display = 'block';
        }
    });

});