// 请求模块 参数1 对象类型的data 参数2 地址后缀
function request(data, url) {
    return new Promise((resolve, reject) => {
        // console.log(data);
        const xhr = new XMLHttpRequest();
        xhr.open('POST', `http://localhost:8000${url}`);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded'); // 后台需要的是json类型的字符串
        xhr.send(JSON.stringify(data));
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    let response = JSON.parse(xhr.response);
                    resolve(response);
                }
            }
        }
    });
}

// 二进制数据传输
function requestImg(data, url) {
    return new Promise((resolve, reject) => {
        // console.log(data);
        const xhr = new XMLHttpRequest();
        xhr.open('POST', `http://localhost:8000${url}`);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded'); // 后台需要的是json类型的字符串
        xhr.send(data);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    let response = JSON.parse(xhr.response);
                    resolve(response);
                }
            }
        }
    });
}