// 引入mysql模块
var mysql = require('mysql');
// 建立与数据库的连接关系
var config = {
    host: '127.0.0.1',
    user: 'root',
    password: 'admin123',
    database: 'patent-project'
}

// 暴露
exports.mysql = function() {
    var connection = mysql.createConnection(config);
    connection.on('error', err => {
        connection.destroy();
        connection = mysql.createConnection(config)
    })
    return connection
};