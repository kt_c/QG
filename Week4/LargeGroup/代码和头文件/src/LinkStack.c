#include "binary_sort_tree.h"
#include <stdio.h>
#include <stdlib.h>

//初始化栈
Status initLStack(LinkStack* s) {
	// 栈顶指针所指的结点并不存放数据
	LinkStackPtr head = (LinkStackPtr)malloc(sizeof(StackNode));
	head->next = NULL;
	s->top = head;
	return 1;
}

//判断栈是否为空
Status isEmptyLStack(LinkStack* s) {
	LinkStackPtr head = s->top;
	if (head->next == NULL)
	{
		return 1;// 栈为空
	}
	else
	{
		return 0;// 栈不为空
	}
}

//入栈
Status pushLStack(LinkStack* s, ElemType2 data) {
	LinkStackPtr head = s->top;
	StackNode* p = (StackNode*)malloc(sizeof(StackNode));
	p->data = data;
	if (head->next == NULL)
	{
		head->next = p;
		p->next = NULL;
		LStackLength(s, &(s->count));
		return 1;
	}
	else
	{
		StackNode* p0 = head->next;
		head->next = p;
		p->next = p0;
		LStackLength(s, &(s->count));
		return 1;
	}
}

//检测栈长度
Status LStackLength(LinkStack* s, int* length) {
	if (s->top->next == NULL)
	{
		*length = 0;
		return 1;
	}
	else
	{
		LinkStackPtr head = s->top;
		StackNode* p = head->next;
		*length = 0;
		while (p)
		{
			(*length)++;
			p = p->next;
		}
		return 1;
	}
}

//出栈
Status popLStack(LinkStack* s, ElemType2* data) {
	LinkStackPtr head = s->top;
	StackNode* p = head->next;
	head->next = p->next;
	*data = p->data;
	free(p);
	LStackLength(s, &(s->count));
	return 1;
}

//得到栈顶元素
Status getTopLStack(LinkStack* s, ElemType2* e) {
	LinkStackPtr head = s->top;
	StackNode* p = head->next;
	*e = p->data;
	return 1;
}

//清空栈
Status clearLStack(LinkStack* s) {
	LinkStackPtr head = s->top;
	if (head->next == NULL)
	{
		LStackLength(s, &(s->count));
		return 1;
	}
	else
	{
		StackNode* p = head->next;
		StackNode* p0 = head;
		while (p)
		{
			p0 = p->next;
			free(p);
			p = p0;
		}
		head->next = NULL;
		LStackLength(s, &(s->count));
		return 1;
	}
}

//销毁栈
Status destroyLStack(LinkStack* s) {
	clearLStack(s);
	LinkStackPtr head = s->top;
	free(head);
	s->top = NULL;
	return 1;
}