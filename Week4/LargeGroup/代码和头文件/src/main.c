#include "binary_sort_tree.h"
#include <stdio.h>
#include <stdlib.h>

void menu()
{
	printf("\t**********树**********\n\t* 1.  初始化\n\t* 2.  添加节点\n\t* 3.  删除结点\n\t* 4.  查找结点\n");
	printf("\t* 5.  非递归前序遍历\n\t* 6.  递归前序遍历\n\t* 7.  非递归中序遍历\n\t* 8.  递归中序遍历\n");
	printf("\t* 9.  非递归后序遍历\n\t* 10. 递归后序遍历\n\t* 11. 层次遍历\n\t* 0.  退出系统\n");
	printf("\t**********************\n请输入：");
}

int main()
{
	BinarySortTree* head = (BinarySortTree*)malloc(sizeof(BinarySortTree));
	if (head == NULL) return false;
	head->root = NULL;
	while (1)
	{
		int num;
		system("cls");
		menu();
		num = intInput();
		switch (num)
		{
		case(1):
		{
			if (BST_init(head))	printf("树初始化成功!\n");
			else printf("树初始化失败！\n");
			system("pause");
			break;
		}
		case(2):
		{
			// i记录循环次数 n是循环总次数
			int i,n;
			// n承载结点的value
			ElemType e;
			printf("请输入想要添加结点的个数:");
			n = intInput();
			for (i = 0; i < n; i++)
			{
				while (1)
				{
					printf("请输入第%d个数据(整形)：", i + 1);
					e = intInput();
					if (BST_insert(head, e))
					{
						printf("第%d个数据添加成功!\n", i + 1);
						break;
					}
					else printf("二叉树中已有相同大小的数据，请重新输入！\n");
				}
			}
			system("pause");
			break;
		}
		case(3):
		{
			if (head->root == NULL)	printf("请先初始化二叉树！\n");
			else
			{
				ElemType e;
				printf("请输入你想要删除的结点的值:");
				e = intInput();
				if (BST_delete(head, e)) printf("删除成功!\n");
				else printf("该值不存在于此二叉树中！\n");
			}
			system("pause");

			break;
		}
		case(4):
		{
			ElemType e;
			printf("请输入你想要查找的结点的值：\n");
			e = intInput();
			if (BST_search(head, e)) printf("这个结点存在于二叉树中！\n");
			else printf("这个结点在二叉树中不存在！\n");
			system("pause");
			break;
		}
		case(5):
		{
			if (head->root == NULL) printf("请先初始化二叉树！\n");
			else
			{
				printf("非递归前序遍历:\n");
				BST_preorderI(head, visit);
			}
			system("pause");
			break;
		}
		case(6):
		{
			if (head->root == NULL) printf("请先初始化二叉树！\n");
			else
			{
				printf("递归前序遍历:\n");
				BST_preorderR(head, visit);
			}
			system("pause");
			break;
		}
		case(7):
		{
			if (head->root == NULL) printf("请先初始化二叉树！\n");
			else
			{
				printf("非递归中序遍历:\n");
				BST_inorderI(head, visit);
			}
			system("pause");
			break;
		}
		case(8):
		{
			if (head->root == NULL) printf("请先初始化二叉树！\n");
			else
			{
				printf("递归中序遍历:\n");
				BST_inorderR(head, visit);
			}
			system("pause");
			break;
		}
		case(9):
		{
			if (head->root == NULL) printf("请先初始化二叉树！\n");
			else
			{
				printf("非递归后序遍历:\n");
				BST_postorderI(head, visit);
			}
			system("pause");
			break;
		}
		case(10):
		{
			if (head->root == NULL) printf("请先初始化二叉树！\n");
			else
			{
				printf("递归后序遍历:\n");
				BST_postorderR(head, visit);
			}
			system("pause");
			break;
		}
		case(11):
		{
			if (head->root == NULL) printf("请先初始化二叉树！\n");
			else
			{
				printf("层次遍历:\n");
				BST_levelOrder(head, visit);
			}
			system("pause");
			break;
		}
		default:
		{
			printf("输入错误，请重新输入！\n");
			system("pause");
			break;
		}
		case(0):exit(0);
		}
	}
	return 0;
}