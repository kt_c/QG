#include "binary_sort_tree.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * BST initialize
 * @param BinarySortTreePtr BST
 * @return is complete
 */
 // 初始化
Status BST_init(BinarySortTreePtr head)
{
	ElemType e;
	printf("请输入一个整型数据来初始化二叉树:");
	e = intInput();
	// 初始化仅仅创建一个根节点 根节点存放数据 根节点指向为空
	head->root = (Node*)malloc(sizeof(Node));
	if (head->root == NULL) return false;
	head->root->value = e;
	head->root->left = head->root->right = NULL;
	return true;
}

// 通过值找到匹配的最底部
Node* findLeaf(NodePtr root, ElemType e)
{
	// 通过递归 将每一个传进来的参数都当做根节点 向左或者向右前进
	// 根节点大于新数据
	if (root->value > e)
	{
		if (root->left != NULL) findLeaf(root->left, e);
		else return root;
	}
	// 根节点小于新数据
	else if (root->value < e)
	{
		if (root->right != NULL) findLeaf(root->right, e);
		else return root;
	}
	// 根节点等于新数据
	else return false;
}

/**
 * BST insert
 * @param BinarySortTreePtr BST
 * @param ElemType value to insert
 * @return is successful
 */
 // 插入
Status BST_insert(BinarySortTreePtr head, ElemType e)
{
	// 没有根节点 后期或许可以判断一下说没有初始化
	if (head->root == NULL)
	{
		head->root = (Node*)malloc(sizeof(Node));
		head->root->value = e;
		head->root->left = NULL;
		head->root->right = NULL;
		return true;
	}
	Node* root = head->root;
	// 寻找应被插入的结点
	Node* leaf=findLeaf(root, e);
	// 新数据与根节点大小相同
	if (leaf == NULL) return false;
	if (leaf->value > e)
	{
		// 创建左节点
		leaf->left = (Node*)malloc(sizeof(Node));
		if (leaf->left == NULL) return false;
		leaf->left->value = e;
		leaf->left->left = NULL;
		leaf->left->right = NULL;
		return true;
	}
	else
	{
		// 创建右节点
		leaf->right = (Node*)malloc(sizeof(Node));
		if (leaf->right == NULL) return false;
		leaf->right->value = e;
		leaf->right->left = NULL;
		leaf->right->right = NULL;
		return true;
	}
}

// 根据数值用递归的方法进行查找
Status findValue(Node* root, ElemType e)
{
	// 结束的标志为 查找到相等的值 或者 两边为NULL
	if (root->value == e) return true;
	if (root->left == NULL && root->right == NULL) return false;
	else
	{
		// 往左边走
		if (root->value > e && root->left != NULL) findValue(root->left, e);
		// 往右边走
		else if (root->value < e && root->right != NULL) findValue(root->right, e);
	}
}

/**
 * BST search
 * @param BinarySortTreePtr BST
 * @param ElemType the value to search
 * @return is exist 是否存在
 */
 // 查找
Status BST_search(BinarySortTreePtr head, ElemType e)
{
	Node* root = head->root;
	if (findValue(root, e)) return true;
	else return false;
}

/**
 * BST delete
 * @param BinarySortTreePtr BST
 * @param ElemType the value for Node which will be deleted
 * @return is successful
 */
 // 删除
Status BST_delete(BinarySortTreePtr head, ElemType e)
{
	// i用来记录front相对after是往左还是往右 往左 0  往右 1
	int i = 0;
	// 没有此结点就直接退出
	if (!findValue(head->root, e)) return false;
	Node* front = head->root, * after = head->root;
	// 找到所在结点及前一个结点 有可能是根节点的
	while (front->value != e)
	{
		after = front;
		if (front->value > e)
		{
			front = front->left;
			i = 0;
		}
		else
		{
			front = front->right;
			i = 1;
		}
	}
	// 叶子结点
	if (front->left == NULL && front->right == NULL)
	{
		// 只有一个根节点
		if (front == head->root)
		{
			head->root = NULL;
			free(front);
			return true;
		}
		// front 在左
		else if (i == 0)
		{
			after->left = NULL;
			free(front);
			return true;
		}
		// front 在右
		else
		{
			after->right = NULL;
			free(front);
			return true;
		}
	}
	// 只有右孩子的结点
	else if (front->left == NULL)
	{
		// 根节点只有右孩子
		if (head->root == front)
		{
			head->root = front->right;
			free(front);
			return true;
		}
		// front在左 front孩子在右
		else if (i == 0)
		{
			after->left = front->right;
			free(front);
			return true;
		}
		// front在右 front孩子在右
		else
		{
			after->right = front->right;
			free(front);
			return true;
		}
	}
	// 只有左孩子的结点
	else if (front->right == NULL)
	{
		// 根节点只有左孩子
		if (head->root == front)
		{
			head->root = front->left;
			free(front);
			return true;
		}
		// front在左 front孩子在左
		else if (i == 0)
		{
			after->left = front->left;
			free(front);
			return true;
		}
		// front在右 front孩子在左
		else
		{
			after->right = front->left;
			free(front);
			return true;
		}
	}
	// 左右孩子都有的结点 直接前驱 寻找front左孩子中的最大值
	else
	{
		Node* max = front->left;
		Node* beforeMax = max;
		// 找front左孩子中的最大值
		while (max->right != NULL)
		{
			beforeMax = max;
			max = max->right;
		}
		// max就是front的左结点
		if (max == front->left)
		{
			max->right = front->right;
			// front是根节点
			if (front == head->root)
			{
				head->root = max;
				free(front);
				return true;
			}
			// front在after左边
			else if (i == 0)
			{
				after->left = max;
				free(front);
				return true;
			}
			// front在after右边
			else if (i == 1)
			{
				after->right = max;
				free(front);
				return true;
			}
		}
		// max不是front左结点 用max取代front
		else
		{
			beforeMax->right = NULL;
			max->left = front->left;
			max->right = front->right;
			// front是根节点
			if (front == head->root)
			{
				head->root = max;
				free(front);
				return true;
			}
			// front在after左边
			else if (i == 0)
			{
				after->left = max;
				free(front);
				return true;
			}
			// front在after右边
			else if (i == 1)
			{
				after->right = max;
				free(front);
				return true;
			}
		}
	}
}

// 打印函数
void visit(NodePtr p)
{
	printf("%d ", p->value);
}

// 递归中序遍历函数的实现者
Status BST_inorderR_realize(NodePtr p, void(*visit)(NodePtr p))
{
	if (p == NULL) return true;
	BST_inorderR_realize(p->left, visit);
	visit(p);
	BST_inorderR_realize(p->right, visit);
}

/**
 * BST inorder traversal with recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
 // 递归中序遍历函数的调用者
Status BST_inorderR(BinarySortTreePtr head, void (*visit)(NodePtr p))
{
	BST_inorderR_realize(head->root, visit);
	return true;
}

// 递归前序遍历函数的实现者
Status BST_preorderR_realize(NodePtr p, void (*visit)(NodePtr p))
{
	if (p == NULL) return true;
	visit(p);
	BST_preorderR_realize(p->left, visit);
	BST_preorderR_realize(p->right, visit);
}


/**
 * BST preorder traversal with recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
 // 递归前序遍历函数的调用者
Status BST_preorderR(BinarySortTreePtr head, void (*visit)(NodePtr))
{
	BST_preorderR_realize(head->root, visit);
	return true;
}

// 递归后序遍历函数的实现者
Status BST_postorderR_realize(NodePtr p, void(*visit)(NodePtr p))
{
	if (p == NULL) return true;
	BST_postorderR_realize(p->left, visit);
	BST_postorderR_realize(p->right, visit);
	visit(p);
}

/**
 * BST postorder traversal with recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
 // 递归后序遍历函数的调用者
Status BST_postorderR(BinarySortTreePtr head, void (*visit)(NodePtr p))
{
	BST_postorderR_realize(head->root, visit);
	return true;
}

/**
 * BST preorder traversal without recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
 // 非递归前序遍历
Status BST_preorderI(BinarySortTreePtr head, void (*visit)(NodePtr p))
{
	// 定义此树节点进行遍历
	Node* temp = head->root;
	LinkStack* s = (LinkStack*)malloc(sizeof(LinkStack));
	if (s == NULL) return false;
	// 初始化栈
	initLStack(s);
	while (temp != NULL || !isEmptyLStack(s))
	{
		// 先遍历左孩子 并入栈
		while (temp != NULL)
		{
			visit(temp);
			pushLStack(s, temp);
			temp = temp->left;
		}
		// 左孩子遍历完成 取栈顶 找右孩子 此时循环还没有结束 再遍历它的左孩子 直至孩子全部遍历结束
		if (!isEmptyLStack(s))
		{
			popLStack(s, &temp);
			temp = temp->right;
		}
	}
	// 销毁栈
	destroyLStack(s);
	return true;
}

/**
 * BST inorder traversal without recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
 // 非递归中序遍历
Status BST_inorderI(BinarySortTreePtr head, void (*visit)(NodePtr p))
{
	Node* temp = head->root;
	LinkStack* s = (LinkStack*)malloc(sizeof(LinkStack));
	if (s == NULL) return false;
	// 初始化栈
	initLStack(s);
	// 先把左孩子入栈 所有左孩子入栈结束
	while (temp != NULL || !isEmptyLStack(s)){
		while (temp != NULL)
		{
			pushLStack(s, temp);
			temp = temp->left;
		}
		// 左孩子入栈结束 取栈顶 输出栈顶元素 遍历右孩子
		if(!isEmptyLStack(s))
		{
			popLStack(s, &temp);
			visit(temp);
			temp = temp->right;
		}
	}
	destroyLStack(s);
	return true;
}

/**
 * BST preorder traversal without recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
 // 非递归后序遍历
Status BST_postorderI(BinarySortTreePtr head, void (*visit)(NodePtr p))
{
	Node* p = head->root,*cur=p;
	LinkStack* s = (LinkStack*)malloc(sizeof(LinkStack));
	if (s == NULL) return false;
	// 初始化栈
	initLStack(s);
	// 根节点进栈
	pushLStack(s, p);
	// 栈不为空
	while (!isEmptyLStack(s))
	{
		getTopLStack(s, &p);
		if ((p->left == NULL && p->right == NULL) || (cur == p->left || cur == p->right))
		{
			visit(p);
			popLStack(s, &p);
			cur = p;
		}
		else
		{
			if (p->right != NULL)
			{
				pushLStack(s, p->right);
			}
			if (p->left != NULL)
			{
				pushLStack(s, p->left);
			}
		}

	}
	destroyLStack(s);
	return true;
}

/**
 * BST level order traversal
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
 // 层次遍历
Status BST_levelOrder(BinarySortTreePtr head, void (*visit)(NodePtr p))
{
	LQueue* Q = (LQueue*)malloc(sizeof(LQueue));
	if (Q == NULL) return false;
	// 初始化队列
	InitLQueue(Q);
	Node* p = head->root;
	if (head->root == NULL) return false;
	EnLQueue(Q, p);
	while (!IsEmptyLQueue(Q))
	{
		GetHeadLQueue(Q, &p);
		DeLQueue(Q);
		visit(p);
		if (p->left) EnLQueue(Q, p->left);
		if (p->right) EnLQueue(Q, p->right);
	}
	DestoryLQueue(Q);
	return true;
}

/*int main()
{
	ElemType e = 0;
	ElemType n = 0, k = 3, t = 1;
	BinarySortTree* head = (BinarySortTree*)malloc(sizeof(BinarySortTree));
	if (head == NULL) return false;
	head->root = NULL;
	// 初始化
	if (BST_init(head)) printf("success init\n");
	else printf("fail init\n");
	// 插入一个结点
	if (BST_insert(head, e)) printf("success insertnode\n");
	else printf("fail insertnode\n");
	// 查找结点
	if (BST_search(head, e)) printf("success findnode\n");
	else printf("fail findnode\n");
	// 删除结点
	if (BST_delete(head, e)) printf("success deletenode\n");
	else printf("fail deletenode\n");
	// 大量插入结点
	while (n!= 10)
	{
		e = n * k - t * 3;
		if (BST_insert(head, e)) printf("success insertnode\n");
		else printf("fail insertnode\n");
		n++;
		t++;
		k++;
	}
	// 递归中序遍历
	BST_inorderR(head, visit);
	printf("\n");
	// 递归前序遍历
	BST_preorderR(head, visit);
	printf("\n");
	// 递归后序遍历
	BST_postorderR(head, visit);
	printf("\n");
	// 非递归前序遍历
	BST_preorderI(head, visit);
	printf("\n");
	// 非递归中序遍历
	BST_inorderI(head, visit);
	printf("\n");
	// 非递归后序遍历
	BST_postorderI(head, visit);
	printf("\n");
	// 层次遍历
	BST_levelOrder(head, visit);
	printf("\n");
	return 0;
}*/
