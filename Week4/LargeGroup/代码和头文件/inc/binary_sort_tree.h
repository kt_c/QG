//
// Created by eke_l on 2021/4/21.
//

#ifndef BINARYSORTTREE_BINARY_SORT_TREE_H
#define BINARYSORTTREE_BINARY_SORT_TREE_H

#define true 1
#define false 0
#define succeed 1
#define failed 0
#define TRUE 1
#define FALSE 0
#define Status int

typedef int ElemType;


// 二叉树
typedef struct Node {
    ElemType value;
    struct Node* left, * right;
}Node, * NodePtr;

typedef struct BinarySortTree {
    NodePtr root;
} BinarySortTree, * BinarySortTreePtr;



// 链栈
typedef NodePtr ElemType2;

typedef  struct StackNode
{
	ElemType2 data;
	struct StackNode* next;
}StackNode, * LinkStackPtr;

typedef  struct  LinkStack
{
	LinkStackPtr top;// 栈顶指针
	int	count;       // 栈中元素个数
}LinkStack;


// 队列
typedef struct node
{
    ElemType2 data;                 //数据域指针
    struct node* next;            //指向当前结点的下一结点
} LQueueNode;

typedef struct Lqueue
{
    LQueueNode* front;                   //队头
    LQueueNode* rear;                    //队尾
    int length;            //队列长度
} LQueue;

char type;// 存放单个数据类型
char datatype[30]; // 用于存放一条队列的数据类型 从小到大依次为从front 到rear
int pprint;


// 函数声明
/**
 * BST initialize
 * @param BinarySortTreePtr BST
 * @return is complete
 */
Status BST_init(BinarySortTreePtr head);// 初始化

/**
 * BST insert
 * @param BinarySortTreePtr BST
 * @param ElemType value to insert
 * @return is successful
 */
Status BST_insert(BinarySortTreePtr head, ElemType e);// 插入

/**
 * BST delete
 * @param BinarySortTreePtr BST
 * @param ElemType the value for Node which will be deleted
 * @return is successful
 */
Status BST_delete(BinarySortTreePtr head, ElemType e);// 删除

/**
 * BST search
 * @param BinarySortTreePtr BST
 * @param ElemType the value to search
 * @return is exist
 */
Status BST_search(BinarySortTreePtr head, ElemType e);// 查找

/**
 * BST preorder traversal without recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
Status BST_preorderI(BinarySortTreePtr head, void (*visit)(NodePtr p));// 非递归前序遍历

/**
 * BST preorder traversal with recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
Status BST_preorderR(BinarySortTreePtr head, void (*visit)(NodePtr p));// 递归前序遍历

/**
 * BST inorder traversal without recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */ 
Status BST_inorderI(BinarySortTreePtr head, void (*visit)(NodePtr p));// 非递归中序遍历

/**
 * BST inorder traversal with recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
Status BST_inorderR(BinarySortTreePtr head, void (*visit)(NodePtr p));// 递归中序遍历

/**
 * BST preorder traversal without recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
Status BST_postorderI(BinarySortTreePtr head, void (*visit)(NodePtr p));// 非递归后序遍历

/**
 * BST postorder traversal with recursion
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
Status BST_postorderR(BinarySortTreePtr head, void (*visit)(NodePtr p));// 递归后序遍历

/**
 * BST level order traversal
 * @param BinarySortTreePtr BST
 * @param (*visit) callback
 * @return is successful
 */
Status BST_levelOrder(BinarySortTreePtr head, void (*visit)(NodePtr p));// 层次遍历

// 打印函数
void visit(NodePtr p);


//链栈
Status initLStack(LinkStack* s);//初始化栈
Status isEmptyLStack(LinkStack* s);//判断栈是否为空
Status getTopLStack(LinkStack* s, ElemType2* e);//得到栈顶元素
Status clearLStack(LinkStack* s);//清空栈
Status destroyLStack(LinkStack* s);//销毁栈
Status LStackLength(LinkStack* s, int* length);//检测栈长度
Status pushLStack(LinkStack* s, ElemType2* data);//入栈
Status popLStack(LinkStack* s, ElemType2 data);//出栈


// 队列
void InitLQueue(LQueue* Q);// 初始化队列
void DestoryLQueue(LQueue* Q);// 销毁队列
Status IsEmptyLQueue(const LQueue* Q);// 检查队列是否为空
Status GetHeadLQueue(LQueue* Q, ElemType2* p);// 查看对头元素
int LengthLQueue(LQueue* Q);// 确定队列长度
Status EnLQueue(LQueue* Q, ElemType2 p);// 入队操作
Status DeLQueue(LQueue* Q);// 出队操作
void ClearLQueue(LQueue* Q);// 清空队列
int intInput();// 整形输入纠错
Status haveNotInitLQueue(LQueue* Q);// 队列初始化检验

#endif //BINARYSORTTREE_BINARY_SORT_TREE_H