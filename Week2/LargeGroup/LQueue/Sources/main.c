#include "LQueue.h"
#include <stdio.h>
#include <stdlib.h>

// 主菜单
void menu()
{
	printf("\t*******队列******\n");
	printf("\t* 1. 初始化队列\n\t* 2. 入队\n\t* 3. 出队\n\t* 4. 检测队列长度\n\t* 5. 遍历队列\n");
	printf("\t* 6. 清空队列\n\t* 7. 销毁队列\n\t* 0. 退出系统\n");
	printf("\t*****************\n请输入：");
}

// 入队菜单
void menuEnLQueue()
{
	printf("* 1. 整形\n* 2. 浮点型\n* 3. 字符型\n");
	printf("请选择入队的数据类型：\n");
}



int main()
{
	LQueue* Q = (LQueue*)malloc(sizeof(LQueue));
	Q->front = Q->rear = NULL;
	Q->length = 0;
	int num;
	while (1)
	{
		system("cls");
		menu();
		num = intInput();
		switch (num)
		{
		case(1):// 初始化
		{
			InitLQueue(Q); break;
		}
		case(2):// 入队
		{
			if (haveNotInitLQueue(Q)) break;
			system("cls");
			int num;
			pprint = LengthLQueue(Q);
			menuEnLQueue();
			num = intInput();
			switch (num)
			{
			case(1):// 整形
			{
				type = 'i';
				datatype[pprint] = 'i';
				int data;
				printf("请输入一个整形数据：");
				data = intInput();// 这个检验函数不行
				if (EnLQueue(Q, &data))
				{
					printf("入队成功\n");
					system("pause");
				}
				break;
			}
			case(2):// 浮点型
			{
				type = 'd';
				datatype[pprint] = 'd';
				double data;
				printf("请输入一个浮点型数据：");
				data = doubleInput();// 这个检验函数不行
				if (EnLQueue(Q, &data))
				{
					printf("入队成功\n");
					system("pause");
				}
				break;
			}
			case(3):// 字符型
			{
				type = 'c';
				datatype[pprint] = 'c';
				char data;
				printf("请输入一个字符型数据：");
				data = charInput();// 这个检验函数不行
				if (EnLQueue(Q, &data))
				{
					printf("入队成功\n");
					system("pause");
				}
				break;
			}
			}
			break;
		}
		case(3):// 出队
		{
			if (haveNotInitLQueue(Q)) break;
			if (IsEmptyLQueue(Q))
			{
				printf("队列为空\n"); 
				system("pause");
				break;
			}
			void* e = malloc(8);
			GetHeadLQueue(Q, e);// 输出头元素
			if (datatype[0] == 'i')// 设计使datatype元素位置随着出队而前移
			{
				printf("%d", *(int*)e);
			}
			else if (datatype[0] == 'd')
			{
				printf("%.2f", *(double*)e);
			}
			else if (datatype[0] == 'c')
			{
				printf("%c", *(char*)e);
			}
			if (DeLQueue(Q))// 出队
			{
				printf("出队成功\n");
				system("pause");
			}
			break;
		}
		case(4):// 检测队列长度
		{
			if (haveNotInitLQueue(Q)) break;
			printf("队列的长度是%d\n", LengthLQueue(Q));
			system("pause");
			break;
		}
		case(5):
		{
			if (haveNotInitLQueue(Q)) break;
			if (IsEmptyLQueue(Q))
			{
				printf("队列为空\n");
				system("pause");
				break;
			}
			if (TraverseLQueue(Q, *LPrint))
			{
				printf("以上为队列中所有内容\n");
				system("pause");
			}
			break;
		}
		case(6):// 清空队列
		{
			if (haveNotInitLQueue(Q)) break;
			ClearLQueue(Q);
			printf("队列已清空\n");
			system("pause"); break;
		}
		case(7):// 销毁队列
		{
			if (haveNotInitLQueue(Q)) break;
			DestoryLQueue(Q);
			printf("队列销毁成功\n");
			system("pause");
			break;
		}
		case(0):exit(0);// 退出系统
		}
	}
}

/*int main()
{
	pprint = 0;
	double data2 = 3.14;// 此数据用于测试入队
	char data1 = 'q';
	void* e;
	e = (void*)malloc(sizeof(21));// 可以转换类型为void* 但是长度不能是void 因为这是不确定的
	LQueue* Q = (LQueue*)malloc(sizeof(LQueue));
	Q->front = Q->rear = NULL;
	Q->length = 0;
	InitLQueue(Q);// 初始化
	if (IsEmptyLQueue(Q))
	{
		printf("空队列\n");
		printf("队列长度为%d\n", LengthLQueue(Q));
	}
	datatype[0] = 'c';
	if (EnLQueue(Q, &data1))// 入队
	{
		printf("入队成功\n");
		printf("队列长度为%d\n", LengthLQueue(Q));
	}
	datatype[1] = 'd';
	if (EnLQueue(Q, &data2))// 入队
	{
		printf("入队成功\n");
		printf("队列长度为%d\n", LengthLQueue(Q));
	}
	
TraverseLQueue(Q, *LPrint);
ClearLQueue(Q);// 清空队列
printf("%d\n", LengthLQueue(Q));
DestoryLQueue(Q);// 销毁队列
return 0;
}*/
