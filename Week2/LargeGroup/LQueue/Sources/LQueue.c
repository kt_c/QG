#include "LQueue.h" 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 初始化队列
void InitLQueue(LQueue* Q) {
	Node* head = (Node*)malloc(sizeof(Node));
	head->next = NULL;
	head->data = NULL;
	Q->front = Q->rear = head;
	printf("初始化成功\n");
	system("pause");
}

// 入队操作
Status EnLQueue(LQueue* Q, void* data) {
	Node* p = (Node*)malloc(sizeof(Node));
	if (!p)
	{
		return FALSE;
	}
	// 就像p->next那样 这里也要开辟一个新的空间 用来存放数据
	if (datatype[pprint] == 'i')
	{
		p->data = (void*)malloc(sizeof(int));
		if (!p) return FALSE;
		memcpy(p->data, data, sizeof(int));
	}
	else if (datatype[pprint] == 'd')
	{
		p->data = (void*)malloc(sizeof(double));
		if (!p) return FALSE;
		memcpy(p->data, data, sizeof(double));
	}
	else if (datatype[pprint] == 'c')
	{
		p->data = (void*)malloc(sizeof(char));
		if (!p) return FALSE;
		memcpy(p->data, data, sizeof(char));
	}
	// p->data = (void*)malloc(21);
	/*if (!p->data)
	{
		return FALSE;
	}
	// 为什么要相差1 ？
	memcpy(p->data, data, 20); // 把data这个地址里面放的内容copy一份到p->data这个地址的空间*/
	p->next = NULL;
	Q->rear->next = p;
	Q->rear = p;
	Q->length++;
	return TRUE;
}

// 确定队列长度
int LengthLQueue(LQueue* Q) {
	return Q->length;
}

// 检查队列是否为空
Status IsEmptyLQueue(const LQueue* Q) {
	if (Q->rear == Q->front)
	{
		return TRUE;
	}
	else return FALSE;
}

// 将数组中每一个元素前移一位
Status arrayFallBack(LQueue* Q,char* arr)
{
	// char* a = arr;
	int i = 0;
	for (; i <= Q->length; i++)
	{
		arr[i] = arr[i + 1];
	}
}

// 出队操作
Status DeLQueue(LQueue* Q) {
	// Q->head->next 是第一个存放数据的节点  Q->head 不存放数据
	if (IsEmptyLQueue(Q))
	{
		return FALSE;
	}
	Node* p = Q->front->next;
	Q->front->next = p->next;
	if (Q->rear == p)
	{
		Q->rear = Q->front;
	}
	// p->data所指的空间需不需要free掉?
	free(p);
	arrayFallBack(Q,datatype);
	Q->length--;
	return TRUE;
}

// 查看头元素
// 形参是void*类型 意味着传任何类型的指针都可以
Status GetHeadLQueue(LQueue* Q, void* e) {
	if (IsEmptyLQueue(Q))
	{
		return FALSE;
	}
	// LPrint(Q->front->next->data);
	int typeSize;
	if (datatype[0] == 'i')
	{
		typeSize = sizeof(int);
		// memcpy(e, Q->front->next->data, typeSize);
		// printf("队头元素为%d\n", *(int*)e);
	}
	else if(datatype[0]=='d')
	{
		typeSize = sizeof(double);
		// memcpy(e, Q->front->next->data, typeSize);
		// printf("队头元素为%.2f\n", *(double*)e);
	}
	else if (datatype[0] == 'c')
	{
		typeSize = sizeof(char);
		// memcpy(e, Q->front->next->data, typeSize);
		// printf("队头元素为%c\n", *(char*)e);
	}
	else
	{
		// 都不是的情况下 直接用最大的 就是字符串的情况
		typeSize = sizeof(Q->front->next->data);
	}
	memcpy(e, Q->front->next->data, typeSize);
	return TRUE;
}

// 销毁队列
void DestoryLQueue(LQueue* Q) {
	ClearLQueue(Q);
	free(Q->front);
	Q->front = Q->rear = NULL;
}

// 清空队列
void ClearLQueue(LQueue* Q) {
	while (DeLQueue(Q));
}

// 遍历函数
Status TraverseLQueue(const LQueue* Q, void (*foo)(void* q)) {
	if (IsEmptyLQueue(Q))
	{
		return FALSE;
	}
	pprint = 0;
	Node* head = Q->front;
	Node* p=head;
	for (p = Q->front->next; p; p = p->next)
	{
		foo(p->data);
	}
	return TRUE;
}

// 打印指针q所指的内容
void LPrint(void* q) 
{
	if (datatype[pprint] == 'i')
	{
		printf("%d->", *(int*)q);
		pprint++;
	}
	else if (datatype[pprint] == 'd')
	{
		printf("%.2f->", *(double*)q);
		pprint++;
	}
	else if (datatype[pprint] == 'c')
	{
		printf("%c->", *(char*)q);
		pprint++;
	}
}

// 整形输入纠错
int intInput()
{
	int i, num;
	char ch;
	while (1)
	{
		i = scanf_s("%d", &num);
		while ((ch = getchar()) != '\n' && ch != EOF);
		if (i == 1) break;
		else
		{
			printf("\n输入错误，请重新输入\n");
			continue;
		}
	}
	return num;
}

// 浮点型形输入纠错
double doubleInput()
{
	int i;
	double num;
	char ch;
	while (1)
	{
		i = scanf_s("%lf", &num);// 谨记谨记 double输入用的是lf
		while ((ch = getchar()) != '\n' && ch != EOF);
		if (i == 1) break;
		else
		{
			printf("\n输入错误，请重新输入\n");
			continue;
		}
	}
	return num;
}

// 字符型形输入纠错
char charInput()
{
	int i;
	char c;
	char ch;
	while (1)
	{
		i = scanf_s("%c", &c);
		while ((ch = getchar()) != '\n' && ch != EOF);
		if (i == 1) break;
		else
		{
			printf("\n输入错误，请重新输入\n");
			continue;
		}
	}
	return c;
}

// 初始化检验
Status haveNotInitLQueue(LQueue* Q)
{
	if (Q->front == NULL)
	{
		printf("尚未初始化队列\n");
		system("pause");
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/**************************************************************
 *    End-Multi-Include-Prevent Section
 **************************************************************/



