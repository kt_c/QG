#include "LinkStack.h"
#include <stdio.h>
#include <stdlib.h>



Status menu()
{
	printf("*****链栈实现*****\n* 1. 栈初始化\n* 2. 入栈\n* 3. 出栈\n* 4. 检测栈长度\n");
	printf("* 5. 清空栈\n* 6. 销毁栈\n* 0 .退出系统\n请选择：");
	return 1;
}

int intInput()// 整形输入纠错
{
	int i, num;
	char ch;
	while (1)
	{
		i = scanf_s("%d", &num);
		while ((ch = getchar()) != '\n' && ch != EOF);
		if (i == 1) break;
		else
		{
			printf("\n输入错误，请重新输入\n");
			continue;
		}
	}
	return num;
}

ElemType ElemInput()// ElemType输入纠错
{
	int i;
	ElemType e;
	char ch;
	while (1)
	{
		i = scanf_s("%d", &e);
		while ((ch = getchar()) != '\n' && ch != EOF);
		if (i == 1) break;
		else
		{
			printf("输入错误，请重新输入\n");
			continue;
		}
	}
	return e;
}

// 判断栈是否初始化
Status haveNotInitLStack(LinkStack* s)
{
	if (s->top == NULL)
	{
		printf("请先初始化栈\n");
		system("pause");
		return 1;
	}
	else return 0;
}


int main()
{
	LinkStack* s = (LinkStack*)malloc(sizeof(LinkStack));
	s->top = NULL;
	s->count = 0;
	int num;
	while (1)
	{
		system("cls");
		menu();
		num = intInput();
		switch (num)
		{
		case(1):// 栈初始化
		{
			if (initLStack(s))
			{
				printf("栈初始化成功\n");
			}
			else
			{
				printf("栈初始化失败\n");
			}
			system("pause");
			break;
		}
		case(2):// 入栈
		{
			if (haveNotInitLStack(s)) break;
			ElemType e;
			printf("请输入入栈的ElemType类型数据:\n");
			e = ElemInput();
			if (pushLStack(s, e))
			{
				printf("入栈成功\n");
				system("pause");
			}
			break;
		}
		case(3):// 出栈
		{
			if (haveNotInitLStack(s)) break;
			if (isEmptyLStack(s)) break;
			ElemType data;
			if (popLStack(s, &data))
			{
				printf("出栈数据为%d\n", data);
				system("pause");
			}
			break;
		}
		case(4):// 检测栈长度
		{
			if (haveNotInitLStack(s)) break;
			printf("栈长度为%d\n", s->count);
			system("pause");
			break;
		}
		case(5):// 清空栈
		{
			if (haveNotInitLStack(s)) break;
			if (clearLStack(s))
			{
				printf("已清空\n");
				system("pause");
			}
			break;
		}
		case(6):// 销毁栈
		{
			if (haveNotInitLStack(s)) break;
			if (destroyLStack(s))
			{
				printf("销毁栈成功\n");
				system("pause");
				break;
			}
		}
		case(0):
			exit(0);
		}
	}
	return 0;
}