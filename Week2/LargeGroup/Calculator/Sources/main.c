#include <stdio.h>
#include <stdlib.h>
#include "Calculator.h"

// 获取优先级
int getRank(char c)
{
	if (c == '*' || c == '/')
	{
		return 2;
	}
	else if (c == '+' || c == '-')
	{
		return 1;
	}
	return 0;
}

// 运算符 是否入栈 递归函数
Status isHigher(LinkStack* s, char c)
{
	char data;
	if (isEmptyLStack(s))// 栈为空 运算符直接入栈
	{
		charPushLStack(s, c);
		printf("运算符入栈成功\n");
		return SUCCESS;
	}
	charGetTopLStack(s, &data);
	if (getRank(c) > getRank(data))// 运算符优先级大 入栈
	{
		charPushLStack(s, c);
		printf("运算符入栈成功\n");
		return SUCCESS;
	}
	else// 运算符优先级小 出栈 递归
	{
		charPopLStack(s, &postfix[i]);
		i++;
		isHigher(s, c);// 递归调用
	}
}

// 计算
Status cal(LinkStack* s, char c)
{
	ElemType a;
	ElemType b;
	ElemType d;
	popLStack(s, &b);// 先出栈
	popLStack(s, &a);// 后出栈
	if (c == '+')
	{
		d = a + b;
	}
	else if (c == '-')
	{
		d = a - b;
	}
	else if (c == '*')
	{
		d = a * b;
	}
	else if (c == '/')
	{
		d = a / b;
	}
	pushLStack(s,d);
	return 1;
}

int main()
{
	int result;
	char* p = infix;
	LinkStack* s = (LinkStack*)malloc(sizeof(LinkStack));
	if (!s) return 1;
	initLStack(s);
	printf("请以英文输入法输入!!!\n");
	printf("仅支持个位数的计算!!!\n");
	printf("请以数字开头!!!\n");
	printf("请输入运算式:\n");
	gets(infix);

	// 中缀表达式转化为后缀表达式
	for (p=infix,i=0; *p!=0; p++)
	{
		if (*p>='0'&&*p<='9')// 数字转化
		{
			postfix[i] = *p;
			i++;
			printf("数字成功转化\n");
		}
		else if (*p == '(')// 左括号直接入栈
		{
			charPushLStack(s, '(');
			printf("'(' 成功入栈\n");
		}
		else if (*p == ')')// 右括号 
		{
			char data;
			while (1)// 运算符出栈 直至遇到左括号
			{
				charGetTopLStack(s, &data);
				if (data == '(')
				{
					charPopLStack(s, &data);
					break;
				}
				charPopLStack(s, &postfix[i]);
				i++;
			}
		}
		else if (*p == '+' || *p == '-' || *p == '*' || *p == '/')// 运算符
		{
			isHigher(s, *p);
		}
	}
	while (!isEmptyLStack(s))
	{
		charPopLStack(s, &postfix[i]);
		i++;
	}
	postfix[i] = 0;
	printf("%s\n", infix);
	printf("%s\n", postfix);

	// 后缀表达式计算
	i = 0;
	for (i = 0; postfix[i] != 0; i++)
	{
		if (postfix[i] >= '0' && postfix[i] <= '9')
		{
			ElemType data = (ElemType*)(postfix[i] - '0');
			pushLStack(s, data);
		}
		else
		{
			cal(s,postfix[i]);
		}
	}
	getTopLStack(s, &result);
	printf("%d",result);
	return 0;
}