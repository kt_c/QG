#ifndef CALCULATOR_H_INCLUDED
#define CALCULATOR_H_INCLUDED

typedef enum Status
{
	ERROR = 0,
	SUCCESS = 1
} Status;

typedef int ElemType;

typedef  struct StackNode
{
	ElemType data;
	char operator;
	struct StackNode* next;
}StackNode, * LinkStackPtr;

typedef  struct  LinkStack
{
	LinkStackPtr top;// 栈顶指针
	int	count;       // 栈中元素个数
}LinkStack;

#define SIZE_C 100 // 计算式的规模限制

char infix[SIZE_C];// 承接中缀运算式
char postfix[SIZE_C];// 承接后缀表达式
int i;// 用来计算后缀表达式的长度

//链栈
Status initLStack(LinkStack* s);//初始化栈
Status isEmptyLStack(LinkStack* s);//判断栈是否为空
Status getTopLStack(LinkStack* s, ElemType* e);//得到栈顶元素
Status clearLStack(LinkStack* s);//清空栈
Status destroyLStack(LinkStack* s);//销毁栈
Status LStackLength(LinkStack* s, int* length);//检测栈长度
Status pushLStack(LinkStack* s, ElemType data);//入栈
Status popLStack(LinkStack* s, ElemType* data);//出栈

Status charPushLStack(LinkStack* s, char data);// 运算符入栈

Status charPopLStack(LinkStack* s, char* data);// 运算符出栈

Status charGetTopLStack(LinkStack* s, char* e);// 运算符得到栈顶元素

// 传的都是二级指针
#endif 