window.onload = function() {
    // 注意避免重复注册

    let edit = document.querySelector('#edit'); // 编辑键
    let footer = document.querySelector('footer'); // 底部删除栏
    let deleteClose = document.querySelector('.deleteClose'); // 删除栏的关闭按钮
    let checks = document.querySelectorAll('article .note input'); // 复选框
    let add = document.querySelector('.add'); // 添加键
    let adding = document.querySelector('.adding'); // 添加框
    let addTop = document.querySelector('.Top'); // 添加框的头部
    let addClose = document.querySelector('.Top .Close'); // 添加框的删除按钮
    let addtextarea = document.querySelector('.adding textarea'); // 添加框的文本域
    let set = document.querySelector('.adding input'); // 添加框的保存按钮
    let bodyMask = document.querySelector('.bodyMask'); // 背景遮罩层
    let article = document.querySelector('article'); // 内容区
    let notes = document.querySelectorAll('.note'); // 单条笔记区域
    let texts = document.querySelectorAll('.note .text'); // 笔记文本区域

    // 将数组存放到本地存储中
    let notepad = new Array(); // notepad[0]不进行存储
    notepad.push(0);

    let n = 0; // n用于记录正在进行操作的笔记,n从1开始进行计算 在修改文本的时候涉及n的操作

    // 打开页面时将本地数据添加到页面上
    function start() {
        let val;
        notepad = localStorage.getItem('notepad');
        console.log(`${notepad}`);
    }

    start();

    // 编辑框的弹出与删除
    edit.addEventListener('click', editEject);

    function editEject() {
        for (let i = 0; i < checks.length; i++) {
            // 重新点开设置为全不选中
            checks[i].checked = false; // 注意这里的false不需要加引号
            checks[i].style.display = 'inline-block';
            footer.style.display = 'block';
        }
    }

    deleteClose.addEventListener('click', function() {
        for (let i = 0; i < checks.length; i++) {
            checks[i].style.display = 'none';
            footer.style.display = 'none';
        }
    });

    // 文本框的弹出移动与删除
    // 点击加号弹出添加框
    add.addEventListener('click', function() {
        addtextarea.value = ``;
        adding.style.display = 'block';
        bodyMask.style.display = 'block';
    });

    addClose.addEventListener('click', function() {
        adding.style.display = 'none';
        bodyMask.style.display = 'none';
        addtextarea.value = ``;
    });

    // 模态框
    // 鼠标按下获取鼠标在盒子内的坐标
    addTop.addEventListener('mousedown', function(e) {
        let x = e.pageX - adding.offsetLeft;
        let y = e.pageY - adding.offsetTop;
        document.addEventListener('mousemove', move);
        // 将鼠标的位置减去鼠标在盒子内的位置的值赋给盒子
        function move(e) {
            adding.style.left = e.pageX - x + 'px';
            adding.style.top = e.pageY - y + 'px';
            // 如何无bug解决鼠标嵌进边边之后模态框出不来的问题
        }
        // 鼠标离开解除鼠标移动事件
        document.addEventListener('mouseup', function() {
            document.removeEventListener('mousemove', move);
        });
    });

    // 添加新文本
    // 实验解除绑定 解除绑定后再点击也是没用的了 是真的一次性按钮=> 保存跟修改分开吧 虽然代码会多点
    set.addEventListener('click', addText);

    function addText() {
        console.log('hello');
        let val = addtextarea.value;
        // 本地存储
        console.log(notepad.length);
        notepad.push(val);
        localStorage.setItem('notepad', notepad);
        // 这里不做texts的重新获取 因为数量还没有变化 数量有变化的时候记得立刻重新获取
        // 文本插入到最前面
        texts[0].innerHTML = val;
        let note = notes[0].cloneNode(true); // 此note尽在此块内生效 暂时性变量
        if (notes.length === 1) article.appendChild(note);
        else {
            article.insertBefore(note, notes[1]);
        }
        // notes checks 的数量变化 重新获取
        notes = document.querySelectorAll('.note');
        checks = document.querySelectorAll('.article .note input');
        notes[1].classList = 'note';
        addtextarea.val = ``;
        adding.style.display = 'none';
        bodyMask.style.display = 'none';
    }

    // // 变量声明
    // let texts = document.querySelectorAll('.note .text');
    // let edit = document.querySelector('#edit');
    // let checks = document.querySelectorAll('article .note input');
    // let footer = document.querySelector('footer');
    // let deleteClose = document.querySelector('.deleteClose');
    // let Close = document.querySelector('.Top .Close');
    // let adding = document.querySelector('.adding');
    // let Top = document.querySelector('.Top');
    // let bodyMask = document.querySelector('.bodyMask');
    // let add = document.querySelector('.add');
    // let notes = document.querySelectorAll('.note');
    // let set = document.querySelector('.adding input');
    // let textarea = document.querySelector('.adding textarea');
    // let article = document.querySelector('article');
    // let mould = document.querySelector('.mould');
    // let n = storageLength(); // text是从1开始的
    // let dustbin = document.querySelector('.dustbin');

    // // 打开页面时将本地数据添加到页面上
    // function start() {
    //     let val;
    //     for (let i = 1; localStorage.getItem(`text${i}`); i++) {
    //         val = localStorage.getItem(`text${i}`);
    //         // 重新获取notes
    //         notes = document.querySelectorAll('.note');
    //         // 给模板赋值
    //         texts[0].innerHTML = val;
    //         // 新文本插入到最前面
    //         let note = notes[0].cloneNode(true);
    //         article.insertBefore(note, notes[1]);
    //         // 获取最新note节点并修改成显示状态
    //         notes = document.querySelectorAll('.note');
    //         notes[1].classList = 'note';
    //         // 获取最新note节点并添加 编辑弹出框事件
    //         checks = document.querySelectorAll('article .note input');
    //         edit.addEventListener('click', editEject);
    //     }
    // }

    // // 检测本地数据中text的数量
    // function storageLength() {
    //     let i = 0;
    //     for (i = 1; localStorage.getItem(`text${i}`); i++);
    //     return i - 1;
    // }

    // start();

    // // 编辑框的弹出与删除
    // edit.addEventListener('click', editEject);

    // function editEject() {
    //     for (let i = 0; i < checks.length; i++) {
    //         // 重新点开设置为全不选中
    //         checks[i].checked = false; // 注意这里的false不需要加引号
    //         checks[i].style.display = 'inline-block';
    //         footer.style.display = 'block';
    //     }
    // }

    // deleteClose.addEventListener('click', function() {
    //     for (let i = 0; i < checks.length; i++) {
    //         checks[i].style.display = 'none';
    //         footer.style.display = 'none';
    //     }
    // });

    // // 文本框的弹出移动与删除
    // // 点击加号弹出添加框
    // add.addEventListener('click', function() {
    //     textarea.value = ``;
    //     adding.style.display = 'block';
    //     bodyMask.style.display = 'block';
    //     set.addEventListener('click', addText);
    // });

    // Close.addEventListener('click', function() {
    //     adding.style.display = 'none';
    //     bodyMask.style.display = 'none';
    //     textarea.value = ``;
    // });

    // // 模态框
    // // 鼠标按下获取鼠标在盒子内的坐标
    // Top.addEventListener('mousedown', function(e) {
    //     let x = e.pageX - adding.offsetLeft;
    //     let y = e.pageY - adding.offsetTop;
    //     document.addEventListener('mousemove', move);
    //     // 将鼠标的位置减去鼠标在盒子内的位置的值赋给盒子
    //     function move(e) {
    //         adding.style.left = e.pageX - x + 'px';
    //         adding.style.top = e.pageY - y + 'px';
    //         // 如何无bug解决鼠标嵌进边边之后模态框出不来的问题
    //     }
    //     // 鼠标离开解除鼠标移动事件
    //     document.addEventListener('mouseup', function() {
    //         document.removeEventListener('mousemove', move);
    //     });
    // });

    // // 添加新文本 功能实现
    // function addText() {
    //     let val = textarea.value;
    //     // 不输入则不添加
    //     if (val != ``) {
    //         // 本地存储
    //         n = storageLength();
    //         localStorage.setItem(`text${++n}`, val);
    //         // 写入文本
    //         let texts = document.querySelectorAll('.note .text');
    //         texts[0].innerHTML = val;
    //         // 新文本插入到最前面
    //         let note = notes[0].cloneNode(true);
    //         article.insertBefore(note, notes[1]);
    //         // 获取最新note节点并修改成显示状态
    //         notes = document.querySelectorAll('.note');
    //         notes[1].classList = 'note';
    //         // 获取最新note节点并添加 编辑弹出框事件
    //         checks = document.querySelectorAll('article .note input');
    //         edit.addEventListener('click', editEject);
    //     }
    //     textarea.value = ``;
    //     adding.style.display = 'none';
    //     bodyMask.style.display = 'none';
    //     notes = document.querySelectorAll('.note');
    //     set.removeEventListener('click', addText);
    // }

    // // 删除功能

    // dustbin.addEventListener('click', function() {
    //     // 关闭删除弹出框
    //     for (let i = 0; i < checks.length; i++) {
    //         checks[i].style.display = 'none';
    //         footer.style.display = 'none';
    //     }
    //     for (let i = 1; i < checks.length; i++) {
    //         if (checks[i].checked == true) {
    //             // 移除DOM数树中的相应数据
    //             article.removeChild(notes[i]);
    //             // 清空本地存储的text数据
    //             for (let i = 1; localStorage.getItem(`text${i}`); i++) {
    //                 localStorage.removeItem(`text${i}`);
    //             }
    //             // 重新编排本地存储的数据
    //             notes = document.querySelectorAll('.note');
    //             texts = document.querySelectorAll('.note .text');
    //             for (let i = 1; i < notes.length; i++) {
    //                 let val = texts[i].innerHTML;
    //                 localStorage.setItem(`text${notes.length-i}`, val);
    //             }
    //         }
    //     }
    // });

    // // 修改部分
    // // 函数可以写在前面，但是这一部分的执行要放在后面，因为在前面页面尚没有加载出notes的时候notes.length为1
    // // 现在要做的是 在点开笔记之后 注册一个新的保存键的点击事件 不生成新的节点 
    // // 并且要给叉号添加 移除点击事件

    // let i = 0;

    // function reviseText() {
    //     let val = textarea.value;
    //     localStorage.setItem(`text${notes.length-i}`, val);
    //     adding.style.display = 'none';
    //     bodyMask.style.display = 'none';
    //     console.log('success');
    //     set.removeEventListener('click', reviseText);
    // }

    // for (i = 1; i < notes.length; i++) {
    //     console.log((`${notes.length}`));
    //     // 为什么这里需要重新获取texts
    //     texts = document.querySelectorAll('.note .text');
    //     texts[i].addEventListener('click', function() {
    //         // 文本框弹出
    //         adding.style.display = 'block';
    //         bodyMask.style.display = 'block';
    //         // 信息填充
    //         textarea.value = texts[i].innerHTML;
    //         set.addEventListener('click', reviseText);
    //     });
    // }




    // let textarea = document.querySelector('textarea');
    // let set = document.querySelector("#set");
    // let get = document.querySelector('#get');
    // let del = document.querySelector('#del');
    // let clear = document.querySelector('#clear');
    // let i = 0;
    // set.addEventListener('click', function() {
    //     let val = textarea.value;
    //     // 在这里给不同文本赋予不同的值 采用数字变化的方法 是可以的 
    //     // 根据本地存储的数量来确定数字的大小
    //     // 用什么办法来确定本地存储的数量
    //     localStorage.setItem(`uname${i++}`, val);
    // });
    // get.addEventListener('click', function() {
    //     let val = localStorage.getItem('uname');
    //     console.log(val);
    //     textarea.value = localStorage.getItem(`uname0`); // 动态添加文本
    // });
    // del.addEventListener('click', function() {
    //     localStorage.removeItem(`uname`); // 对指定key进行删除 因此每一个文本需要有不同的key
    // });
    // clear.addEventListener('click', function() {
    //     localStorage.clear();
    // });
}