window.onload = function() {
    // 避免重复注册 那么就要有单独的一个修改按钮 并且修改按钮自己注册 不在其他点击事件里面进行注册
    // 点击修改按钮时获取当前正在操作的笔记的数字 依次来对数组进行操作
    // 改用用数组对笔记的内容进行存储 数组的内容是对象
    // 这样可以挺方便的用数组的方法对数组进行增删改查

    let n = 0; // 记录正在进行修改的笔记

    // 变量获取
    let edit = document.querySelector('#edit'); // 编辑键
    let checks = document.querySelectorAll('.note input'); // 所有的复选框
    let footer = document.querySelector('footer'); // 编辑框
    let editClose = document.querySelector('.deleteClose'); // 编辑框关闭
    let dustbin = document.querySelector('.dustbin'); // 编辑框删除键
    let add = document.querySelector('.add'); // 添加键
    let adding = document.querySelector('.adding'); // 添加框
    let bodyMask = document.querySelector('.bodyMask'); // 背景遮罩
    let addClose = document.querySelector('.addClose'); // 添加框关闭
    let addTop = document.querySelector('.addTop'); // 添加框顶部
    let set = document.querySelector('#set'); // 保存键
    let setText = document.querySelector('#setText'); // 添加框的文本域
    let article = document.querySelector('article'); // 笔记区域
    let notes = document.querySelectorAll('.note'); // 所有的笔记
    let texts = document.querySelectorAll('.text'); // 所有笔记的文本域
    let revise = document.querySelector('.revise'); // 修改框
    let reviseClose = document.querySelector('.reviseClose'); // 修改框关闭
    let reviseTop = document.querySelector('.reviseTop'); // 修改框顶部
    let reviseText = document.querySelector('#reviseText'); // 修改框的文本域
    let revising = document.querySelector('#revising'); // 修改键
    let search = document.querySelector('.search'); // 搜索按钮

    // 将本地存储的信息放入到数组中
    let notepad = new Array(); // 创建空数组
    if (JSON.parse(localStorage.getItem('notepad')) == null) {
        notepad.push(0); // 数组开头放置0 并不参与存放数据 方便操作
    } else {
        notepad = JSON.parse(localStorage.getItem('notepad'));
        notepad[0] = 0;
    }
    // console.log(notepad);

    // 设置对象
    function NOTE(content) {
        this.content = content;
    }

    // 将数组中存放的信息添加到DOM 树中并进行显示
    // console.log(notepad[1].content);
    for (let i = 1; i < notepad.length; i++) {
        texts[0].innerHTML = notepad[i].content;
        let note = notes[0].cloneNode(true);
        note.classList = 'note';
        if (notes.length == 1) {
            article.appendChild(note);
        } else {
            article.insertBefore(note, notes[1]);
        }
        // 重新获取notes texts checks
        notes = document.querySelectorAll('.note');
        texts = document.querySelectorAll('.text');
        checks = document.querySelectorAll('.note input');
    }

    /****************************************************************************/

    // 点击编辑键
    edit.addEventListener('click', function() {
        footer.style.display = 'block';
        for (let i = 0; i < checks.length; i++) {
            checks[i].style.display = 'inline-block';
        }
    });

    // 点击编辑框的关闭键
    editClose.addEventListener('click', function() {
        footer.style.display = 'none';
        for (let i = 0; i < checks[i].length; i++) {
            checks[i].style.display = 'none';
            // 点击了关闭之后就统一将复选框变为不选中的状态
            checks[i].checked = false;
        }
    });

    // 点击编辑框的删除键
    dustbin.addEventListener('click', function() {
        // 删除DOM树中的相应数据
        for (let i = 1; i < notepad.length; i++) {
            if (checks[i].checked == true) {
                article.removeChild(notes[i]);
            }
        }
        // 重新获取notes texts checks
        notes = document.querySelectorAll('.note');
        texts = document.querySelectorAll('.text');
        checks = document.querySelectorAll('.note input');
        // 初始化数组
        notepad.splice(1, notepad.length - 1);
        // 为数组重新填充数据
        for (let i = notes.length - 1; i > 0; i--) {
            let val = texts[i].innerHTML;
            let content = new NOTE(val);
            notepad.push(content);
        }
        // 将数组转化并存储到本地
        localStorage.setItem('notepad', JSON.stringify(notepad));
        // 隐藏删除框 复选框
        footer.style.display = 'none';
        for (let i = 1; i < notes.length; i++) {
            checks[i].style.display = 'none';
        }
        // 强制刷新
        location.reload(false);
    });

    /***************************************************************/

    // 点击添加键
    add.addEventListener('click', function() {
        bodyMask.style.display = 'block';
        adding.style.display = 'block';
    });

    // 点击添加框的关闭键
    addClose.addEventListener('click', function() {
        bodyMask.style.display = 'none';
        adding.style.display = 'none';
    });

    // 添加框顶部 模态框效果
    // 鼠标按下获取鼠标在盒子内的坐标
    addTop.addEventListener('mousedown', function(e) {
        let x = e.pageX - adding.offsetLeft;
        let y = e.pageY - adding.offsetTop;
        document.addEventListener('mousemove', move);
        // 将鼠标的位置减去鼠标在盒子内的位置的值赋给盒子
        function move(e) {
            adding.style.left = e.pageX - x + 'px';
            adding.style.top = e.pageY - y + 'px';
            // 如何无bug解决鼠标嵌进边边之后模态框出不来的问题
        }
        // 鼠标离开解除鼠标移动事件
        document.addEventListener('mouseup', function() {
            document.removeEventListener('mousemove', move);
        });
    });

    //添加框的保存键
    set.addEventListener('click', function() {
        // 将数据保存到本地
        let val = setText.value;
        let content = new NOTE(val); // 实例化了一个对象content 后续使用内容的时候应该可以直接用数组下标操作
        notepad.push(content); // 在数组中开辟空间存放新的内容
        localStorage.setItem('notepad', JSON.stringify(notepad));
        // 将数据添加到DOM树中并显示
        texts[0].innerHTML = val;
        let note = notes[0].cloneNode(true);
        note.classList = 'note';
        if (notes.length == 1) {
            article.appendChild(note);
        } else {
            article.insertBefore(note, notes[1]);
        }
        // 重新获取notes text checks
        notes = document.querySelectorAll('.note');
        texts = document.querySelectorAll('.text');
        checks = document.querySelectorAll('.note input');
        // 隐藏添加框 背景遮罩 复选框
        adding.style.display = 'none';
        bodyMask.style.display = 'none';
        for (let i = 1; i < notes.length; i++) {
            checks[i].style.display = 'none';
        }
        // 清空添加框文本域
        setText.value = ``;
        // 强制刷新
        location.reload(false);
        // 打印本地存储信息
        // console.log(JSON.parse(localStorage.getItem('notepad')));
        // 打印数组信息
        // console.log(notepad);
    });

    /*********************************************************************/

    // 点击笔记
    for (let i = 1; i < notes.length; i++) {
        texts[i].addEventListener('click', function() {
            revise.style.display = 'block';
            bodyMask.style.display = 'block';
            reviseText.value = texts[i].innerHTML;
            n = i;
        });
    }

    // 点击修改框的关闭键
    reviseClose.addEventListener('click', function() {
        bodyMask.style.display = 'none';
        revise.style.display = 'none';
    });

    // 修改框顶部 模态框效果
    // 鼠标按下获取鼠标在盒子内的坐标
    reviseTop.addEventListener('mousedown', function(e) {
        let x = e.pageX - revise.offsetLeft;
        let y = e.pageY - revise.offsetTop;
        document.addEventListener('mousemove', move);
        // 将鼠标的位置减去鼠标在盒子内的位置的值赋给盒子
        function move(e) {
            revise.style.left = e.pageX - x + 'px';
            revise.style.top = e.pageY - y + 'px';
            // 如何无bug解决鼠标嵌进边边之后模态框出不来的问题
        }
        // 鼠标离开解除鼠标移动事件
        document.addEventListener('mouseup', function() {
            document.removeEventListener('mousemove', move);
        });
    });

    // 点击修改键
    revising.addEventListener('click', function() {
        let i = n;
        // console.log(i);
        let val = reviseText.value;
        // 修改数组中的相应内容 
        notepad[notepad.length - i].content = val;
        // 修改DOM树中的相应内容
        texts[i].innerHTML = val;
        // 将数组重新存储到本地
        localStorage.setItem('notepad', JSON.stringify(notepad));
        // 清空文本域
        reviseText.value = ``;
        // 隐藏修改框 背景遮罩
        bodyMask.style.display = 'none';
        revise.style.display = 'none';
    });

    /*************************************************************/

    // 搜索内容高亮

    // 正则中遇到特殊字符需要转义

    var specChar = ["$", "(", ")", "*", "+", ".", "[", "]", "?", "\\", "^", "{", "}", "|"];

    /** 特殊字符须要转义 */

    function formatForKeyword(keyword) {
        undefined

        if (specChar.indexOf(keyword) > 0) {
            undefined

            keyword = '\\' + keyword;

            return keyword;

        } else {
            undefined

            return keyword;

        }

    }

    /**

    * 高亮显示关键字, 构造函数

    * @param {} colors 颜色数组，其中每个元素是一个 '背景色,前景色' 组合

    */

    var Highlighter = function(colors) {
        undefined

        this.colors = colors;

        if (this.colors == null) {
            undefined

            //默认颜色

            this.colors = ['#ffff00,#000000', '#dae9d1,#000000', '#eabcf4,#000000',

                '#c8e5ef,#000000', '#f3e3cb, #000000', '#e7cfe0,#000000',

                '#c5d1f1,#000000', '#deeee4, #000000', '#b55ed2,#000000',

                '#dcb7a0,#333333', '#7983ab,#000000', '#6894b5, #000000'
            ];

        }

    }

    /**

    * 高亮显示关键字

    * @param {} node html element

    * @param {} keywords 关键字， 多个关键字可以通过空格隔开， 其中每个关键字会以一种颜色显式

    *

    * 用法：

    * var hl = new Highlighter();

    * hl.highlight(document.body, '这个 世界 需要 和平');

    */

    Highlighter.prototype.highlight = function(node, keywords) {
        undefined

        if (!keywords || !node || !node.nodeType || node.nodeType != 1)

            return;

        keywords = this.parsewords(keywords);

        if (keywords == null)

            return;

        for (var i = 0; i < keywords.length; i++) {
            undefined

            this.colorword(node, keywords[i]);

        }

    }

    /**

    * 对所有#text的node进行查找，如果有关键字则进行着色

    * @param {} node 节点

    * @param {} keyword 关键字结构体，包含了关键字、前景色、背景色

    */

    Highlighter.prototype.colorword = function(node, keyword) {
        undefined

        for (var i = 0; i < node.childNodes.length; i++) {
            undefined

            var childNode = node.childNodes[i];

            if (childNode.nodeType == 3) {
                undefined

                //childNode is #text

                var re = new RegExp(keyword.word, 'i');

                if (childNode.data.search(re) == -1) continue;

                re = new RegExp('(' + keyword.word + ')', 'gi');

                var forkNode = document.createElement('span');

                if (childNode.data.indexOf('')) {
                    undefined

                    childNode.data = childNode.data.replaceAll('', '>');

                }

                forkNode.innerHTML = childNode.data.replace(re, '$1');

                node.replaceChild(forkNode, childNode);

            } else if (childNode.nodeType == 1) {
                undefined

                //childNode is element

                if (childNode.nodeName.toLowerCase() == 'style' || childNode.nodeName.toLowerCase() == 'script') {
                    undefined

                    // 遇到script style 不处理

                } else {
                    undefined

                    this.colorword(childNode, keyword);

                }

            }

        }

    }

    /**

    * 将空格分隔开的关键字转换成对象数组

    * @param {} keywords

    * @return {}

    */

    Highlighter.prototype.parsewords = function(keywords) {
        undefined

        keywords = keywords.replace(/\/s+/g, ' ');

        keywords = keywords.split(' ');

        if (keywords == null || keywords.length == 0)

            return null;

        var results = [];

        for (var i = 0; i < keywords.length; i++) {
            undefined

            var keyword = {};

            var color = this.colors[i % this.colors.length].split(',');

            keyword.word = keywords[i];

            keyword.bgColor = color[0];

            keyword.foreColor = color[1];

            results.push(keyword);

        }

        return results;

    }

    /**

    * 按照字符串长度，由长到短进行排序

    * @param {} list 字符串数组

    */

    Highlighter.prototype.sort = function(list) {
        undefined

        list.sort(function(e1, e2) {
            undefined

            return e1.length < e2.length;

        });

    }

    // 使用方法：

    /** 邮件体 高亮显示搜索关键字 */

    function formatForBody(val, keyword) {
        undefined

        if (keyword == null || keyword.length == 0) {
            undefined

            return val;

        } else {
            undefined

            var divDom = document.createElement("div");

            divDom.innerHTML = val;

            if (divDom.innerText.indexOf(keyword) >= 0) { // 是否需要替换

                keyword = formatForKeyword(keyword);

                var hl = new Highlighter();

                hl.highlight(divDom, keyword);

                val = divDom.innerHTML;

            }

            return val;

        }

    }

    search.addEventListener('click', function() {

    });

}