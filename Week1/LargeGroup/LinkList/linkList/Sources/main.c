#define _CRT_SECURE_NO_DEPRECATE
#include<stdio.h>
#include "linkedList.h"
#include<stdlib.h>
#include<conio.h>

int NumInput()// 整形输入纠错
{
	int i,num;
	while (1)
	{
		i = scanf_s("%d", &num);
		setbuf(stdin, NULL);// 清空缓存区，以免无法输入
		if (i == 1) break;
		else
		{
			printf("\n输入错误，请重新输入\n");
			continue;
		}
	}
	return num;
}

ElemType ElemInput()// ElemType 类型输入纠错
{
	int i;
	ElemType e;
	while (1)
	{
		rewind(stdin);
		i = scanf_s("%d", &e);
		getchar();
		rewind(stdin);
		setbuf(stdin, NULL);// 清空缓存区，以免无法输入
		if (i == 1) break;
		else
		{
			printf("\n输入错误，请重新输入\n");
			continue;
		}
		rewind(stdin);
	}
	return e;
}

Status buildList(LinkedList* L) // 创建链表
{
	system("cls");
	LNode* head = NULL;
	LNode* p0 = NULL;
	LNode* p = NULL;
	int i = 0;
	InitList(L);// 初始化链表
	head = *L;
	p0 = head;
	// 创建链表数据
	printf("\n请依次输入五个ElemType类型的数据来创建你的链表\n");
	while (i < 5)
	{
		if (p = (LNode*)malloc(sizeof(LNode)))
		{
			printf("\n输入第%d个数据\n", i + 1);
			p->data = ElemInput();
			p->next = NULL;
			p0->next = p;
			p0 = p;
			i++;
		}
		else
		{
			printf("\n开辟空间失败\n");
		}
	}
	printf("\n创建链表成功\n");
	system("pause");
	return SUCCESS;
}

Status deleteNode(LinkedList head)// 删除结点
{
	LNode* p = head->next;
	int i, length, k = 1;
	ElemType e;
	do
	{
		printf("\n你想要删除第几个数据\n");
		i = NumInput();
		length = findLength(head);
		if (i > length)
		{
			printf("\n链表中没有辣么多数据，请重新选择\n");
			system("pause"); continue;
		}
		else if (i == 0)
		{
			printf("\n输入错误，请重新输入\n");
			system("pause"); continue;
		}
		while (p)
		{
			if (i == 1)// 删除第一个有数据的节点
			{
				DeleteList(head, &e);
				break;
			}
			else if (k + 1 == i)
			{
				DeleteList(p, &e);
				break;
			}
			else
			{
				p = p->next;
				k++;
			}
		}
		break;
	} while (1);
	return SUCCESS;
}

Status SearchList(LinkedList L, ElemType e)// 查找内容为e 的节点
{
	LNode* p = L->next;
	while (p)
	{
		if (p->data == e) return SUCCESS;
		else p = p->next;
	}
	printf("\n没有你想要的节点\n");
	return ERROR;
}


void visit(ElemType e)// 遍历函数的回调函数
{
	printf("\nthis is all the datas\n", e);
}

void TraverseList(LinkedList L, void (*visit)(ElemType e))// 遍历链表
{
	if (L == NULL)
	{
		printf("\n链表为空\n");
	}
	else
	{
		LNode* p = L->next;
		ElemType E = 3;
		while (p)
		{
			printf("%d\n", p->data);
			p = p->next;
		}
		visit(E);
	}
	
}

Status DeleteList(LNode* p, ElemType* e)// 删除结点
{
	LNode* r;
	r = p->next;
	*e = r->data;
	p->next = r->next;
	free(r);
	return SUCCESS;
}

Status InsertList(LNode* p, LNode* q) // 将结点q插入到结点p后
{
	if (p->next == NULL)// p为最后一个结点的情况
	{
		p->next = q;
		q->next = NULL;
		return SUCCESS;
	}
	LNode* r = p->next;
	p->next = q;
	q->next = r;
	return SUCCESS;
}

void DestroyList(LinkedList* L)// 摧毁链表
{
	LNode* p = *L, * p1 = *L;// p1 指向第一个有数据的节点
	while (p)
	{
		p = p1->next;
		free(p1);
		p1 = p;
	}
	free(L);

}

Status InitList(LinkedList* L)// 链表初始化
{
	*L = (LNode*)malloc(sizeof(LNode));
	if (*L == NULL) return ERROR;
	(*L)->next = NULL;
	return SUCCESS;
}

int findLength(LinkedList head)// 查找链表长度
{
	int i = 0;
	LNode* p = head->next;
	while (p)
	{
		i++;
		p = p->next;
	}
	return i;
}

Status insertNode(LinkedList L)// 插入结点
{
	LNode* head = L;
	LNode* p0 = NULL;
	LNode* p = head->next;
	ElemType n;
	int i, k = 1, length;
	printf("\n请输入想要插入ElemType类型的数据：\n");
	n = ElemInput();
	p0 = (LNode*)malloc(sizeof(LNode));// 开辟新结点
	if (p0 == NULL)
	{
		printf("\n开辟空间失败\n");
		system("pause");
		exit(0);
	}
	p0->data = n;
	length = findLength(head);
	do  // 插入结点
	{
		printf("\n想要插入到第几个数据后面：\n");
		i = NumInput();
		if (i > length)
		{
			printf("\n结点数没有辣么多，请重新输入：\n");
			system("pause");
			continue;
		}
		while (p)
		{
			if (i == 0)// 插入成为第一个结点
			{
				InsertList(head, p0);
				break;
			}
			else if (k == i)
			{
				InsertList(p, p0);
				break;
			}
			else
			{
				p = p->next;
				k++;
			}
		}
		break;
	} while (1);
	return SUCCESS;
}

void menu()
{
	printf("**************************************************");
	printf("单链表");
	printf("**************************************************\n\n");
	printf("\t\t\t\t\t\t  1.创建链表\n\n\t\t\t\t\t\t  2.插入结点\n\n");
	printf("\t\t\t\t\t\t  3.删除结点\n\n\t\t\t\t\t\t  4.查找结点\n\n");
	printf("\t\t\t\t\t\t  5.销毁链表\n\n\t\t\t\t\t\t  6.展示链表\n\n");
	printf("\t\t\t\t\t\t  0.退出\n");
	printf("************************************************************");
	printf("************************************************************\n");
}

Status searchNode(LinkedList head)// 查找结点
{
	LNode* p = head->next;// 从有数据的第一个开始
	int i, length, k = 1;
	ElemType e;
	do
	{
		printf("\n你想要查找第几个数据\n");
		i = NumInput();
		length = findLength(head);// 链表长度
		if (i > length)
		{
			printf("\n链表中没有辣么多数据，请重新选择\n");
			system("pause"); continue;
		}
		else if (i == 0)
		{
			printf("\n输入错误，请重新输入\n");
			system("pause"); continue;
		}
		while (p)
		{
			if (k == i)
			{
				printf("%d", p->data);
				break;
			}
			else
			{
				p = p->next;
				k++;
			}
		}
		break;
	} while (1);
	return SUCCESS;
}

int main()
{
	LinkedList* L = NULL;// *head 是head指针的内容 内容为LinkedList 类型 即一级指针
	L = (LinkedList*)malloc(sizeof(LinkedList));
	LNode* head = NULL;
	int num;
	do
	{
		system("cls");
		menu();
		printf("\n请选择:");
		num = NumInput();
		switch (num)
		{
		case(1):
			buildList(L);
			head = *L; break;
		case(2):
			insertNode(head); 
			printf("\n插入数据成功\n"); break;
		case(3):
			deleteNode(head);
			printf("\n成功删除结点\n");
			system("pause"); break;
		case(4):
			searchNode(head);
			system("pause"); break;
		case(5):
			DestroyList(L); 
			printf("\n已成功销毁链表\n");
			system("pause"); break;
		case(6):
			TraverseList(head, *visit);
			system("pause"); break;
		case(0):
			exit(0);
		default:
			printf("\n输入错误，请重新输入！\n");
		}
	} while (1);
	return 0;
}