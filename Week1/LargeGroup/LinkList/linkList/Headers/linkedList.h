/***************************************************************************************
 *	File Name				:	linkedList.h
 *	CopyRight				:	2020 QG Studio
 *	SYSTEM					:   win10
 *	Create Data				:	2020.3.28
 *
 *
 *--------------------------------Revision History--------------------------------------
 *	No	version		Data			Revised By			Item			Description
 *
 *
 ***************************************************************************************/

 /**************************************************************
*	Multi-Include-Prevent Section
**************************************************************/
#ifndef LINKEDLIST_H_INCLUDED  // 防止重复包含和编译
#define LINKEDLIST_H_INCLUDED  // linkedlist_h_included

/**************************************************************
*	Macro Define Section 宏定义部分
**************************************************************/

#define OVERFLOW -1  // overflow 全局变量

/**************************************************************
*	Struct Define Section 结构定义部分
**************************************************************/

// define element type   ElemType 类型为 int    element 要素
typedef int ElemType;

// define struct of linked list   定义链表节点结构
typedef struct LNode {
	ElemType data;
	struct LNode* next;
} LNode, * LinkedList;

// define Status  定义状态
typedef enum Status {
	ERROR,
	SUCCESS
} Status;



/**************************************************************
*	Prototype Declare Section   原型声明部分
**************************************************************/

/**
 *  @name        : Status InitList(LinkList *L);   初始化链表   定义链表
 *	@description : initialize an empty linked list with only the head node without value    初始化链表，其中只有头节点
 *	@param		 : L(the head node)  头节点
 *	@return		 : Status            状态
 *  @notice      : None              无
 */
Status InitList(LinkedList* L);  // 初始化

/**
 *  @name        : void DestroyList(LinkedList *L)   销毁链表
 *	@description : destroy a linked list, free all the nodes
 *	@param		 : L(the head node)
 *	@return		 : None
 *  @notice      : None
 */
void DestroyList(LinkedList* L);

/**
 *  @name        : Status InsertList(LNode *p, LNode *q)  函数原型
 *	@description : insert node q after node p    在p节点后面插入q节点
 *	@param		 : p, q
 *	@return		 : Status
 *  @notice      : None
 */
Status InsertList(LNode* p, LNode* q);

/**
 *  @name        : Status DeleteList(LNode *p, ElemType *e)
 *	@description : delete the first node after the node p and assign its value to e   删除p节点后面的第一个节点并将其值存储在e中
 *	@param		 : p, e  形参
 *	@return		 : Status  返回值
 *  @notice      : None
 */
Status DeleteList(LNode* p, ElemType* e);

/**
 *  @name        : void TraverseList(LinkedList L, void (*visit)(ElemType e))
 *	@description : traverse the linked list and call the funtion visit   遍历链表并回调函数 visit
 *	@param		 : L(the head node), visit
 *	@return		 : None
 *  @notice      : None
 */
void TraverseList(LinkedList L, void (*visit)(ElemType e));

/**
 *  @name        : Status SearchList(LinkedList L, ElemType e)
 *	@description : find the first node in the linked list according to e  找到e所在的第一个节点
 *	@param		 : L(the head node), e
 *	@return		 : Status
 *  @notice      : None
 */
Status SearchList(LinkedList L, ElemType e);

/**
 *  @name        : Status ReverseList(LinkedList *L)
 *	@description : reverse the linked list   反转链表
 *	@param		 : L(the head node)
 *	@return		 : Status
 *  @notice      : None
 */
Status ReverseList(LinkedList* L);

/**
 *  @name        : Status IsLoopList(LinkedList L)
 *	@description : judge whether the linked list is looped  判断链表是否成环
 *	@param		 : L(the head node)
 *	@return		 : Status
 *  @notice      : None
 */
Status IsLoopList(LinkedList L);

/**
 *  @name        : LNode* ReverseEvenList(LinkedList *L)
 *	@description : reverse the nodes which value is an even number in the linked list, input: 1 -> 2 -> 3 -> 4  output: 2 -> 1 -> 4 -> 3   单链表奇偶调换
 *	@param		 : L(the head node)
 *	@return		 : LNode(the new head node) 返回新的头节点
 *  @notice      : choose to finish
 */
LNode* ReverseEvenList(LinkedList* L);

/**
 *  @name        : LNode* FindMidNode(LinkedList *L)
 *	@description : find the middle node in the linked list  找到单链表中点
 *	@param		 : L(the head node)
 *	@return		 : LNode
 *  @notice      : choose to finish
 */
LNode* FindMidNode(LinkedList* L);

void menu();// 菜单
void visit(ElemType e);
Status buildList(LinkedList* L);// 创建链表
int findLength(LinkedList head);// 查找链表长度
Status insertNode(LinkedList L);// 插入结点
Status deleteNode(LinkedList head);// 删除结点
Status searchNode(LinkedList head);// 查找结点
ElemType ElemInput();// 识别纠错
int NumInput();// 识别纠错

/**************************************************************
*	End-Multi-Include-Prevent Section
**************************************************************/
#endif
