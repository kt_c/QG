<a name="FYGYM"></a>
# Git Bash
1.  1Unix 与 Linux 风格的命令行
1. 基本命令：
- cd：改变目录
- cd ..：回退到上一个目录，直接cd进入默认目录
- pwd：显示当前所在的目录路径
- ls(ll)：都是列出当前目录中的所有文件，只不过ll（两个ll）列出的内容更为详细
- touch：新建一个文件，如touch index.js 就会在当前目录下新建一个index.js 文件
- rm：删除一个文件，rm index.js 就会把index.js 文件删除
- mkdir：新建一个目录，就是新建一个文件夹
- rm -r：删除一个文件夹，rm-r src 删除src 目录
- rm -rf / ：删除电脑中所有文件，而一切皆文件，连系统都给你删了
- mv：移动文件，mv index.html src index.html 是我们要移动的文件，src 是目标文件夹，当然，这样写，必须保证文件与目标文件夹在同一目录下
- reset：重新初始化终端、清屏
- clear：清屏
- history：查看命令历史
- help：帮助
- exit：退出
- #：代表注释

<a name="PvlDS"></a>
# Git CMD

1. windows 风格的命令行

<a name="tqy6w"></a>
# Git GUI

1. 图形界面的Git

<a name="typs1"></a>
# Git 配置

1. 所有的配置文件，其实都保存在本地
<a name="A33DD"></a>
## 查看配置：

1. 查看配置：git config -l
2. 查看系统config：git config --system --list
2. 查看当前用户配置：git config --global --list

<a name="ynckf"></a>
## Git 相关配置文件

1. Git\etc\gitconfig：Git 安装目录下的gitconfig --system 系统级
1. C:\Users\Administrator\.gitconfig 只适用于当前登录用户的配置 --global 全局
1. 这里可以直接编辑配置文件，通过命令设置后会响应到这里

<a name="Iuh6o"></a>
## 设置用户名与邮箱（用户标识，必要）

1. 当你安装Git 后首先要设置你的用户名和e-mail 地址。
1. 每次Git提交都会使用该信息，它被永远地嵌入到你的提交中
```git
git config --global user.name ""  #名称
git config --global user.email "" #邮箱
```

<a name="N5Rlb"></a>
# Git 基本理论
<a name="qKAo2"></a>
## 工作区域

1. Git 本地有三个工作区域：工作目录（Workinng Directory）、暂存区（Stage/Index）、资源库（Repository或Git Directory）。如果再加上远程的git 仓库（Romote Directory），就可以分为四个工作区
1. 文件在这四个区域间的转换关系如下：

![image.png](https://cdn.nlark.com/yuque/0/2022/png/25604575/1646306920305-ad9a2985-ee83-4a11-aafb-fdaad2bd7633.png#clientId=ufcec5f13-c6d3-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=311&id=u9c6d6738&margin=%5Bobject%20Object%5D&name=image.png&originHeight=311&originWidth=329&originalType=binary&ratio=1&rotation=0&showTitle=false&size=48027&status=done&style=none&taskId=u1b99039f-8a2a-4d1a-940a-2dadff3b1d7&title=&width=329)

- Workspace：工作区，就是你平时存放代码的地方
- Index / Stage：暂存区，用于临时存放你的改动，事实上它只是一个文件，保存即将提交到文件列表的信息
- Repository：仓库区（或本地仓库），就是安全存放数据的位置，这里有你提交到所有的数据。其中HEAD指向最新放入仓库的版本
- Remote：远程仓库，托管代码的服务器，可以简单的认为是你项目组中的一台电脑用于远程数据交换
- 本地的三个仓库确切的说应该是git仓库中HEAD指向的版本

<a name="zzGnR"></a>
## 各文件

1. Directory：使用Git 管理的一个目录，也就是一个仓库，包含我们的工作空间和Git 的管理空间
1. WorkSpace：需要通过Git 进行版本控制的目录和文件，这些目录和文件组成了工作空间
1. .git：存放Git 管理信息的的目录，初始化仓库的时候自动创建（隐藏文件）
1. Index/Stage：暂存区，或者叫待提交更新区，在提交进入repo之前，我们可以把所有的更新放在暂存区
1. Local Ropo：本地仓库，一个存放在本地的版本库；HEAD会只是当前的开发分支（branch）
1. Stash：隐藏，是一个工作状态保存栈，用于保存/恢复WorkSpace中的临时状态

<a name="RTXHa"></a>
## 工作流程

1. 在工作目录中添加、修改文件；
1. 将需要进行版本管理的文件放入暂存区域
1. 将暂存区域的文件提交到git 仓库
1. 因此，git 管理的文件有三种状态：已修改（modified），已暂存（staged），已提交（committed）

<a name="SVnvn"></a>
## Git 项目搭建

1. 工作目录（WorkSpace）一般就是你希望Git 帮助你管理的文件夹，可以是你项目的目录，也可以是一个空目录，建议不要有中文
1. 常用指令：

![image.png](https://cdn.nlark.com/yuque/0/2022/png/25604575/1646309170181-897a124e-4297-4597-8e50-bf5e119e9823.png#clientId=ufcec5f13-c6d3-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=228&id=u6ae57dc8&margin=%5Bobject%20Object%5D&name=image.png&originHeight=228&originWidth=750&originalType=binary&ratio=1&rotation=0&showTitle=false&size=74393&status=done&style=none&taskId=uf8092ae1-aa38-4e74-8a04-1ffec2b12dd&title=&width=750)

<a name="dc2Xd"></a>
### 本地仓库搭建
<a name="wYSEd"></a>
#### 创建全新的仓库

1. 创建全新的仓库，需要用GIT 管理的项目的根目录执行
```git
# 在当前目录创建一个Git 仓库
$ git init
```

2. 执行后可以看到，仅仅在项目目录多出一个.git的隐藏目录，关于版本等的所有信息都在里面

<a name="t8Al1"></a>
#### 克隆远程仓库

1. 将远程服务器上的仓库完全镜像一份到本地
```git
# 克隆一个项目和它的整个代码历史（版本信息）
$ git clone [url]
```

<a name="dWMnj"></a>
## Git 文件操作
<a name="qgtpJ"></a>
### 文件四种状态
![image.png](https://cdn.nlark.com/yuque/0/2022/png/25604575/1646311133737-da7481d4-fb90-49b0-985f-4b471c300b2e.png#clientId=ufcec5f13-c6d3-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=225&id=u4fc2dbf5&margin=%5Bobject%20Object%5D&name=image.png&originHeight=225&originWidth=747&originalType=binary&ratio=1&rotation=0&showTitle=false&size=208460&status=done&style=none&taskId=uab2883c7-d19a-4332-a0e0-e748fd8ff85&title=&width=747)

<a name="o0lQ1"></a>
### 查看文件状态
```git
# 查看指定文件状态
git status [filename]

# 查看所有文件状态
git status

# 添加所有文件到暂存区
git add . 

# 提交暂存区中的内容到本地仓库  -m 提交信息 
git commit -m "信息内容"
```

<a name="HD668"></a>
### 忽略文件

1. 有些时候我们不想把某些文件纳入版本控制中，比如数据库文件，临时文件，设计文件等
1. 在主目录下建立".gitignore"文件，此文件有以下规则：
- 忽略文件中的空行或以井号（#）开始的行将会被忽略
- 可以使用Linux 通配符。例如：星号（*）代表任意多个字符，问号（？）代表一个字符，方括号（[abc]）代表可选字符范围，大括号（{string1，string2，...}）代表可选的字符串等
- 如果名称最前面有一个感叹号（！），表示例外规则，将不被忽略
- 如果名称的最前面是一个路径分隔符（/），表示要忽略的文件在此目录下，而子目录中的文件不被忽略
- 如果名称的最后面是一个路径分隔符（/），表示要忽略的是此目录下该名称的子目录，而非文件（默认文件或目录都忽略）
```git
#为注释
*.txt       # 忽略所有.txt结尾的文件
!lib.txt    # 但lib.txt除外 
/temp       # 仅忽略项目根目录下的TODO文件，不包含其他目录temp
bulid/      # 忽略bulid/ 目录下的所有文件
doc/*.txt   # 会忽略 doc/notes.txt 但不包括 doc/server/srch.txt
```

4. 参考配置：

![image.png](https://cdn.nlark.com/yuque/0/2022/png/25604575/1646313042295-1700c86d-96a9-402c-92bb-7a0862f65e4d.png#clientId=ufcec5f13-c6d3-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=407&id=u81142794&margin=%5Bobject%20Object%5D&name=image.png&originHeight=407&originWidth=201&originalType=binary&ratio=1&rotation=0&showTitle=false&size=43034&status=done&style=none&taskId=u948a53eb-ffe7-441f-9b00-10373aaf93a&title=&width=201)![image.png](https://cdn.nlark.com/yuque/0/2022/png/25604575/1646313031749-53372af6-bb29-499d-9f8c-706154ac2c2c.png#clientId=ufcec5f13-c6d3-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=322&id=u88830d1f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=322&originWidth=201&originalType=binary&ratio=1&rotation=0&showTitle=false&size=30156&status=done&style=none&taskId=u6cd52ae1-2ddc-4fe3-9427-178e55c37f0&title=&width=201)

5. 注意：
- .gitignore 文件从保存那一刻起即生效
- 最好是在一开始就配置 .gitignore 文件，此文件对已经纳入版本管理的文件或者文件夹无效
- 若文件或者文件夹已经纳入版本管理，又想要被忽略，可以进行如下操作：
```git
git rm -r --cached
# 将文件脱出版本管理
```

<a name="X5eTw"></a>
## 配置公钥
```git
# 进入 C:\User\Administrator\.ssh 目录
# 生成公钥 -t rsa 是加密
$ ssh-keygen -t rsa
```

<a name="FrTIj"></a>
## 仓库

1. 开源许可证一般GPL 2.0 或者GPL 3.0 够用

<a name="yoyRk"></a>
## 分支
<a name="BFLFm"></a>
### git分支中常用指令
```git
# 列出所有本地分支
git branch

# 列出所有远程分支
git branch -r

# 新建一个分支，但仍然停留在当前分支
git branch [branch-name]

# 新建一个分支，并切换到该分支
git checkout -b [branch]

# 合并指定分支到当前分支
git merge [branch]

# 删除分支
git branch -d [branch-name]

# 删除远程分支
git push origin --delete [branch-name]
git branch -dr [remote/branch]
```
