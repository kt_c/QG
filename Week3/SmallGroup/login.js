window.onload = function() {
    let username = document.querySelector('#username');
    let password = document.querySelector('#password');
    let loginBtn = document.querySelector('#login-btn');
    let login = document.querySelector('.login');
    let loginSuccess = document.querySelector('.login-success');
    let flag = 1;

    function isNull(str) {
        if (str == '') return true;
        var regu = '^[]+$';
        var re = new RegExp(regu);
        return re.test(str);
    }

    username.addEventListener('blur', function() {
        if (flag == 0) console.log('1');
        username.placeholder = '用户名';
        username.classList = 'username';
    });

    password.addEventListener('blur', function() {
        if (flag == 0) return;
        password.placeholder = '密码';
        password.classList = 'password';
    });

    username.addEventListener('focus', function() {
        username.placeholder = ``;
        username.classList = 'username';
    });

    password.addEventListener('focus', function() {
        password.placeholder = ``;
        password.classList = 'password';
    });

    loginBtn.addEventListener('click', function() {
        flag = 0;
        if (isNull(username.value)) {
            username.placeholder = '用户名不能为空';
            username.classList = 'username error';
        } else if (isNull(password.value)) {
            password.placeholder = '密码不能为空';
            password.classList = 'password error';
        } else {

            let data = new Object();
            data.username = `${username.value}`;
            data.password = `${password.value}`;

            // 发送请求
            const xhr = new XMLHttpRequest();
            xhr.open('POST', 'http://119.91.210.79:3000/login');
            xhr.setRequestHeader('Content-type', 'application/json'); // 后台需要的是json类型的字符串
            xhr.send(JSON.stringify(data));
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200 && xhr.status < 300) {
                        console.log(JSON.parse(xhr.response));
                        if (JSON.parse(xhr.response).status) {
                            login.classList = 'login success';
                            loginSuccess.style.display = 'block';
                            username.value = ``;
                            password.value = ``;
                        } else {
                            alert('密码错误，请重新输入');
                            password.value = ``;
                        }
                    }
                }
            }
        }
        flag = 1;
    });
}