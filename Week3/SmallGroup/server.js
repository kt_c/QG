// 1. 引入express
const { response, request } = require('express');
const express = require('express');

// 2. 创建应用对象
const app = express();

// 3. 创建路由规则
// 1) 接受GET请求
app.get('/server', (request, response) => {
    // 设置响应头  设置允许跨域
    response.setHeader('Access-Control-Allow-Origin', '*');
    // 设置响应
    response.send('hello ajax get');
});

// 2) 接受所有请求 
app.all('/json-server', (request, response) => {
    // 设置响应头  设置允许跨域
    response.setHeader('Access-Control-Allow-Origin', '*');
    // 响应头 同时设置了app.all 以及 Access-Control-Allow-Headers 之后才可以自定义请求头
    response.setHeader('Access-Control-Allow-Headers', '*');
    // 设置响应 里面只接受字符串 因此传数据前需要转换字符串
    const date = {
        name: 'atguigu'
    };
    let str = JSON.stringify(date);
    response.send(str);
});

app.all('/delay', (request, response) => {
    // 设置响应头  设置允许跨域
    response.setHeader('Access-Control-Allow-Origin', '*');
    // 设置定时器 可用于延时测试
    setInterval(function() {
        response.send('HELLO DELAY');
    }, 3000);
});

app.all('/axios-server', (request, response) => {
    // 设置响应头  设置允许跨域
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', '*');
    // 设置响应
    response.send('hello axios post');
});

app.all('/fetch-server', (request, response) => {
    // 设置响应头  设置允许跨域
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', '*');
    // 设置响应
    const data = {
        name: 'atguigu'
    };
    let str = JSON.stringify(data);
    response.send(str);
});

app.all('/jsonp-server', (request, response) => {
    const data = {
        name: 'admin'
    };
    let str = JSON.stringify(data);
    // response.end(`handle(${str})`);
    response.end(`handle(${str})`);
});

app.all('/check-username', (request, response) => {
    const data = {
        exist: 1,
        msg: '当前用户名已存在'
    };
    let str = JSON.stringify(data);
    response.end(`handle(${str})`);
});

app.post('/sors-server', (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', '*');
    response.setHeader('Access-Control-Allow-Method', '*');
    response.send('hello sors');
});

// 4. 监听端口启动服务
app.listen(8000, () => {
    console.log("服务已启动,8000 端口监听中...");
});