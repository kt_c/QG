#include "sort.h"
#include <stdio.h>
#include<stdlib.h>
#include <assert.h>
#include <time.h>

// 整形输入纠错
int intInput()
{
	int i, num;
	char ch;
	while (1)
	{
		i = scanf_s("%d", &num);
		while ((ch = getchar()) != '\n' && ch != EOF);
		if (i == 1) break;
		else
		{
			printf("\n输入错误，请重新输入\n");
			continue;
		}
	}
	return num;
}

void menu()
{
	printf("\t*****排序测试*****\n\t* 1. 进行测试\n\t* 2. 颜色排序\n\t* 3. 寻找第k大的数\n\t* 4. 寻找第k小的数\n\t* 0. 退出系统\n\t******************\n\n请输入：");
}

//利用伪随机数填充文件，伪随机数的范围在RANDMIN~RANDMAX-1之间
void getRandData( int size,int min,int max)
{
	assert(size > 0);

	FILE* fp;
	if (fopen_s(&fp,"testData.txt", "w"))
	{
		printf("文件打开失败！\n");
		exit(0);
	}
	
	int data;

	srand((unsigned)time(NULL));
	int i = 0;
	for (i = 0; i < size; ++i) {
		data = rand() % (max - min) + min;
		fprintf(fp, "%d ", data);// 写入信息到文件中
	}
	fprintf(fp, "\n");
	fclose(fp);
}

// 读取文件信息填充到数组中
void getArray(int* a, int size)
{
	FILE* fp;
	if (fopen_s(&fp, "testData.txt", "r"))
	{
		printf("文件打开失败!\n");
		exit(0);
	}

	int i = 0;
	for(i = 0; i < size; i++)
	{
		fscanf_s(fp, "%d ", &a[i]);// 信息填充到数组中
	}
	fclose(fp);
}

int main()
{
	int num,size,min,max;
	clock_t begin;
	clock_t end;

	while (1)
	{
		system("cls");
		menu();
		num = intInput();
		switch (num)
		{
		case(1):
		{
			while (1)
			{
				system("cls");
				printf("生成数据的数量：");
				size = intInput();
				if (size > 0) break;
				else {
					printf("输入错误，请重新输入!");
					system("pause");
				}
			}
			int* array = (int*)calloc(size, sizeof(int));
			while (1)
			{
				printf("生成的数据的最小值：");
				min = intInput();
				printf("生成数据的最大值：");
				max = intInput();
				if (max >= min) break;
				else {
					printf("输入错误，请重新输入！");
					system("pause");
				}
			}
			if (max == min)
			{
				printf("数组排序完毕，所有排序方法用时为0\n");
				system("pause");
				break;
			}
			getRandData(size,min,max);// 将指定数量指定范围的数据填充到文件

			// 插入排序
			printf("插入排序: ");
			getArray(array, size);
			begin = clock();
			insertSort(array, size);
			end = clock();
			printf("%ldms\n", end - begin);
			isSorted(array, size);
			free(array);


			// 计数排序
			printf("计数排序: ");
			array = (int*)calloc(size, sizeof(int));
			getArray(array, size);
			begin = clock();
			CountSort(array, size, max);
			end = clock();
			printf("%ldms\n", end - begin);
			isSorted(array, size);
			free(array);


			// 快速排序
			printf("快速排序: ");
			array = (int*)calloc(size, sizeof(int));
			getArray(array, size);
			begin = clock();
			QuickSort_Recursion(array, 0, size - 1);
			end = clock();
			printf("%ldms\n", end - begin);
			isSorted(array, size);
			free(array);

			// 归并排序
			printf("归并排序: ");
			array = (int*)calloc(size, sizeof(int));
			getArray(array, size);
			begin = clock();
			MergeSort(array, 0, size-1, array);
			end = clock();
			printf("%ldms\n", end - begin);
			isSorted(array, size);
			free(array);

			// 基数排序
			if (min < 0)
			{
				printf("基数排序暂时无法对负数进行排序！\n");
				system("pause");
				break;
			}
			printf("基数排序: ");
			array = (int*)calloc(size, sizeof(int));
			getArray(array, size);
			begin = clock();
			RadixCountSort(array, size);
			end = clock();
			printf("%ldms\n", end - begin);
			isSorted(array, size);
			free(array);


			system("pause");
			break;
		}
		case(2):
		{
			while (1)
			{
				system("cls");
				printf("生成数据的数量：");
				size = intInput();
				if (size > 0) break;
				else {
					printf("输入错误，请重新输入!");
					system("pause");
				}
			}
			int* array = (int*)calloc(size, sizeof(int));

			getRandData(size, 0, 3);// 将指定数量指定范围的数据填充到文件

			// 颜色排序
			printf("颜色排序: ");
			getArray(array, size);
			begin = clock();
			ColorSort(array, size);
			end = clock();
			printf("%ldms\n", end - begin);
			isSorted(array, size);
			free(array);
		}
		case(3):
		{
			int k;
			while (1)
			{
				system("cls");
				printf("生成数据的数量：");
				size = intInput();
				if (size > 0) break;
				else {
					printf("输入错误，请重新输入!");
					system("pause");
				}
			}
			int* array = (int*)calloc(size, sizeof(int));
			while (1)
			{
				printf("生成的数据的最小值：");
				min = intInput();
				printf("生成数据的最大值：");
				max = intInput();
				if (max >= min) break;
				else {
					printf("输入错误，请重新输入！");
					system("pause");
				}
			}
			while (1)
			{
				printf("寻找第几大的数据：");
				k = intInput();
				if (k > 0) break;
				else
				{
					printf("输入错误，请重新输入！");
					system("pause");
				}
			}
			if (max == min)
			{
				printf("数组中第k大的数是%d\n",max);
				system("pause");
				break;
			}
			getRandData(size, min, max);// 将指定数量指定范围的数据填充到文件
			printf("数组中第k大的数是: ");
			getArray(array, size);
			QuickSort_Recursion(array, 0, size - 1);
			printf("%d\n", array[size-k]);
			free(array);
		}
		case(4):
		{
			int k;
			while (1)
			{
				system("cls");
				printf("生成数据的数量：");
				size = intInput();
				if (size > 0) break;
				else {
					printf("输入错误，请重新输入!");
					system("pause");
				}
			}
			int* array = (int*)calloc(size, sizeof(int));
			while (1)
			{
				printf("生成的数据的最小值：");
				min = intInput();
				printf("生成数据的最大值：");
				max = intInput();
				if (max >= min) break;
				else {
					printf("输入错误，请重新输入！");
					system("pause");
				}
			}
			while (1)
			{
				printf("寻找第几小的数据：");
				k = intInput();
				if (k > 0) break;
				else
				{
					printf("输入错误，请重新输入！");
					system("pause");
				}
			}
			if (max == min)
			{
				printf("数组中第%d小的数是%d\n", k,max);
				system("pause");
				break;
			}
			getRandData(size, min, max);// 将指定数量指定范围的数据填充到文件
			printf("数组中第%d小的数是: ",k);
			getArray(array, size);
			QuickSort_Recursion(array, 0, size - 1);
			printf("%d\n", array[k-1]);
			free(array);
		}
		case(0):
			exit(0);
		default:continue;
		}
	}
}
