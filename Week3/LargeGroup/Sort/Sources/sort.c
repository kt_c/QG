#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include <assert.h>
#include "sort.h"


/**
 *  @name        : void insertSort(int *a,int n);
 *  @description : 插入排序算法
 *  @param       : 数组指针 a, 数组长度 n
 */
void insertSort(int* a, int n)
{
    int i;
    int temp;// 保存要插入的元素
    int j;// 从当前要比较插入的元素的前面一个开始
    for (i = 1; i < n; i++)// 第一个元素视为有序，把后面的元素一个一个的插入前面
    {
        temp = a[i];
        j = i - 1;
        while (j >= 0 && a[j] > temp)
        {
            a[j + 1] = a[j];// 前面的元素往后面移动
            j--;
        }
        a[j + 1] = temp;// 把要插入的元素，插入到对应的位置
    }
}

/**
 *  @name        : void QuickSort_Recursion(int *a, int begin, int end);
 *  @description : 快速排序（递归版）
 *  @param       : 数组指针a，数组起点begin，数组终点end
 */
void QuickSort_Recursion(int* a, int begin, int end)
{
    if (begin > end)
    {
        return;
    }
    int i = begin, j = end, temp = a[i];// 获取左右基准数
    while (i < j)
    {
        while (temp < a[j] && i < j)
        {
            j--;
        }
        if (i < j)
        {
            a[i++] = a[j];
        }
        while (temp > a[i] && i < j)
        {
            i++;
        }
        if (i < j)
        {
            a[j--] = a[i];
        }
    }
    a[i] = temp;

    QuickSort_Recursion(a, begin, i - 1);// 左边
    QuickSort_Recursion(a, i + 1, end);// 右边
}

/**
 *  @name        : void MergeArray(int *a,int begin,int mid,int end,int *temp);
 *  @description : 归并排序（合并数组）
 *  @param       : 数组指针a，数组起点begin，数组中点mid，数组终点end，承载数组指针temp
 */
void MergeArray(int* a, const int begin, const int mid, int end, int* temp)
{
    int *help= (int*)calloc(end-begin+1, sizeof(int));
    int p1 = begin;
    int p2 = mid + 1;
    int i = 0;
    while (p1 <= mid && p2 <= end) {
        help[i++] = a[p1] <= a[p2] ? a[p1++] : a[p2++];
    }
    while (p1 <= mid) {
        help[i++] = a[p1++];
    }
    while (p2 <= end) {
        help[i++] = a[p2++];
    }
    for (i = 0; i < end - begin + 1; i++) {
        a[begin + i] = help[i];
    }
}



/**
 *  @name        : void MergeSort(int *a,int begin,int end,int *temp);
 *  @description : 归并排序
 *  @param       : 数组指针a，数组起点begin，数组终点end，承载数组指针temp
 */
void MergeSort(int* a, const int begin, const int end, int* temp)
{
    if (begin == end) {
        return;
    }
    int mid = begin + ((end - begin) >> 1);
    MergeSort(a, begin, mid,a);
    MergeSort(a, mid + 1, end,a);
    MergeArray(a, begin, mid, end,a);
}

/**
 *  @name        : void CountSort(int *a, int size , int max)
 *  @description : 计数排序
 *  @param       : 数组指针a，数组长度size，数组最大值max
 */
void CountSort(int* a, int size, int max)
{
    assert(a);
    //通过max和min计算出临时数组所需要开辟的空间大小
    int  min = a[0];
    for (int i = 0; i < size; i++) {
        if (a[i] < min)
            min = a[i];
    }
    //使用calloc将数组都初始化为0
    int range = max - min + 1;
    int* b = (int*)calloc(range, sizeof(int));
    //使用临时数组记录原始数组中每个数的个数
    for (int i = 0; i <size; i++) {
        //注意：这里在存储上要在原始数组数值上减去min才不会出现越界问题
        b[a[i] - min] += 1;
    }
    int j = 0;
    //根据统计结果，重新对元素进行回收
    for (int i = 0; i < range; i++) {
        while (b[i]--) {
            //注意：要将i的值加上min才能还原到原始数据
            a[j++] = i + min;
        }
    }
    //释放临时数组
    free(b);
    b = NULL;
}


/**
 *  @name        : void RadixCountSort(int *a,int size)
 *  @description : 基数计数排序
 *  @param       : 数组指针a，数组长度size
 */
void RadixCountSort(int* a, int size)
{
    /*
         1、创建队列
         2、先求最大数字的位数
         3、求出相应位数的值， 并根据位数值将其存储相应的队列中
         4、按顺序输出所有队列中的值，  循环处理3,4步，循环次数由第一步算出
     */

    Que que[10];
    for (int i = 0; i < 10; ++i)
    {
        que[i].data = (int*)malloc(sizeof(int) * size);
        assert(que[i].data != NULL);
        que[i].head = que[i].tail = 0;
    }

    int width = GetMaxWidth(a, size); // O(n)

    for (int i = 0; i < width; ++i)  // O(d * n)
    {
        //  将数组中所有的数字取其相应位数的值，并将其存储到相应的队列中
        for (int j = 0; j < size; ++j)
        {
            int  num = GetNumOfWidth(a[j], i);
            que[num].data[que[num].tail++] = a[j];
        }

        int k = 0;
        for (int m = 0; m < 10; ++m)
        {
            while (que[m].head != que[m].tail)
            {
                a[k++] = que[m].data[que[m].head++];
            }
        }

        for (int n = 0; n < 10; ++n)
        {
            que[n].head = que[n].tail = 0;
        }
    }

    for (int i = 0; i < 10; ++i)
    {
        free(que[i].data);
    }
}

//利用伪随机数填充数组array，伪随机数的范围在RANDMIN~RANDMAX-1之间
void getRandArray(int array[], int size)
{
    assert(array != NULL && size > 0);

    srand((unsigned)time(NULL));
    int i = 0;
    for (i = 0; i < size; ++i) {
        array[i] = rand() % (RANDMAX - RANDMIN) + RANDMIN;
    }
}

//判断数组array是否已经是有序的
void isSorted(int array[], int size)
{
    assert(array != NULL && size > 0);

    int unsorted = 0;
    int i = 0;
    for (i = 1; i < size; ++i) {
        if (array[i] < array[i - 1]) {
            unsorted = 1;
            break;
        }
    }

    if (unsorted) {
        printf("the array is unsorted!\n\n");
    }
    else {
        printf("the array is sorted!\n\n");
    }
}

//  求数组中最大数字的宽度
static int  GetMaxWidth(int* arr, int len)
{
    int max = arr[0];
    for (int i = 0; i < len; ++i)
    {
        if (arr[i] > max)  max = arr[i];
    }

    int width = 0;
    while (max)  //  12
    {
        width++;
        max /= 10;
    }

    return width;
}

//  求data的倒数第i+1位上的值
static int GetNumOfWidth(int data, int i)
{
    while (i)
    {
        data /= 10;
        i--;
    }

    return data % 10;
}

/**
 *  @name        : void ColorSort(int *a,int size)
 *  @description : 颜色排序
 *  @param       : 数组指针a（只含0，1，2元素），数组长度size
 */
void ColorSort(int* a, int size)
{
    int* p1 = a, * p0 = a, * p2 = a + size-1;
    int temp;
    while (1)
    {
        if (*p1 == 1 || *p1 == 2)
        {
            if (p1 == p2) return;
            break;
        }
        p1++;
        p0++;
    }
    for (; p1!=p2; p1++)
    {
        while (1)
        {
            if (*p1 == 2)
            {
                if (p1 == p2) break;
                temp = *p2;
                *p2 = *p1;
                *p1 = temp;
                p2--;
            }
            else if (*p1 == 0)
            {
                if (p1 == p2) break;
                temp = *p0;
                *p0 = *p1;
                *p1 = temp;
                p0++;
            }
            else if (*p1 == 1||p1==p2) break;
        }
        if (p1 == p2) p1--;
    }
    if (*p1 == 2)
    {
        temp = *p2;
        *p2 = *p1;
        *p1 = temp;
        p2--;
    }
    else if (*p1 == 0)
    {
        temp = *p0;
        *p0 = *p1;
        *p1 = temp;
        p0++;
    }
}


